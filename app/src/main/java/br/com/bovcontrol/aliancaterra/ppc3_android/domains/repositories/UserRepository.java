package br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories;

import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;

/**
 * Created by fernanda on 27/09/16.
 */

public interface UserRepository {
    boolean saveIntentiontoAccessNewFeatures(String feature,String userId);
    void sendEmailRecoverPassword(String Email,final FirebaseListener listener);
    boolean updatePasword(String password);

}

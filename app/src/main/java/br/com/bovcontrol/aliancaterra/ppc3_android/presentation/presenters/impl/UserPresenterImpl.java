package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl;

import android.content.Context;

import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.UserRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl.UserRepositoryImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.UserPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.UserView;

/**
 * Created by fernanda on 30/09/16.
 */

public class UserPresenterImpl implements UserPresenter {

    UserView userView;
    UserRepository userRepository;
    Context context;

    public UserPresenterImpl(UserView userView, Context context){
        this.userView=userView;
        userRepository= new UserRepositoryImpl();
        this.context=context;
    }

    @Override
    public void saveIntention(String faseclick) {
       boolean everythingright= userRepository.saveIntentiontoAccessNewFeatures(faseclick, SharedPreferenceManager.getId(context));
        if(everythingright){
            userView.sucessToSaveIntention();
        }else{
            userView.errorToSaveIntention();
        }
    }
}

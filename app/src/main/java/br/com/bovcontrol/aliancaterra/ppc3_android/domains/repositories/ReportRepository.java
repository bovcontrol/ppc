package br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories;

import android.content.Context;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;

/**
 * Created by nic on 04/10/16.
 */

public interface ReportRepository {
    public void getReport(String userId, String farmId,Context context);
}

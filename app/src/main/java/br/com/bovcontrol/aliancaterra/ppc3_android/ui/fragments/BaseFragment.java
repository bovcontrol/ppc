package br.com.bovcontrol.aliancaterra.ppc3_android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import br.com.bovcontrol.aliancaterra.ppc3_android.Util.Resources;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;

/**
 * Created by fernanda on 23/09/16.
 */

public class BaseFragment  extends Fragment {
    private Tracker mTracker;
    Resources res;
    @Override
    public void onResume() {
        super.onResume();
        res= new Resources(getActivity());
        mTracker.setScreenName(res.getLabelInResource("ga_"+getClass().getSimpleName()));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MainApplication application = (MainApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
    }
}

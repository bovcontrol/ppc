package br.com.bovcontrol.aliancaterra.ppc3_android.ui.util;

import android.content.Context;

/**
 * Created by fernanda on 28/09/16.
 */

public class ResourcesUtil {


    public int getResourceId(Context context,String name,String type){
        int resourceId = context.getResources().getIdentifier(name, type, context.getPackageName());
        return resourceId;
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StatusBarUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by fernanda on 21/10/16.
 */

public class DoYouHaveCupomActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnHaveCupom)
    Button btnHaveCupom;
    @BindView(R.id.btnWantCupom)
    Button btnWantCupom;
    @BindView(R.id.tvwihoutCupom)
    TextView tvwihoutCupom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_you_have_cumpom);
        ButterKnife.bind(this);
        createToolbarBackArrow();
        Window window = getWindow();
        StatusBarUtil.changeStatusBar(window, getResources().getColor(R.color.gray));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @OnClick({R.id.btnHaveCupom, R.id.btnWantCupom, R.id.tvwihoutCupom})
    public void onClick(View view) {
        Intent i=null;
        switch (view.getId()) {
            case R.id.btnHaveCupom:
                mobileAnalytics.AddEvent(this,this,"button.havecupom");
                i= new Intent(this,CupomActivity.class);
                break;
            case R.id.btnWantCupom:
                mobileAnalytics.AddEvent(this,this,"button.wantcupom");
                i= new Intent(this,WantCupomActivity.class);
                break;
            case R.id.tvwihoutCupom:
                mobileAnalytics.AddEvent(this,this,"button.withoutcupom");
                i= new Intent(this,PaymentActivity.class);
                break;
        }
        startActivity(i);
    }
}

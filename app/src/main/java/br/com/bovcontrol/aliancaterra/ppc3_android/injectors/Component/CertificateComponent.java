package br.com.bovcontrol.aliancaterra.ppc3_android.injectors.Component;

import javax.inject.Singleton;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.FarmRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.injectors.modules.CertificateModule;
import br.com.bovcontrol.aliancaterra.ppc3_android.injectors.modules.FarmRepositoryModule;
import br.com.bovcontrol.aliancaterra.ppc3_android.injectors.scope.ActivityScope;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.CertificateView;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.CertificateActivity;
import dagger.Component;

/**
 * Created by fernanda on 27/09/16.
 */

@ActivityScope
@Singleton
@Component(modules = CertificateModule.class)
public interface CertificateComponent {
    void inject(CertificateActivity activity);
}

package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views;

/**
 * Created by fernanda on 23/09/16.
 */

public interface BaseView {
   public  void showLoading();
    public void hideLoading();
    public void showErrorMessage(String message);
    public void showWihoutNetwork();
}

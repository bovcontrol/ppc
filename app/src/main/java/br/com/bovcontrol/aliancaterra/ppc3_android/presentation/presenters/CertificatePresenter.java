package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters;

import android.content.Context;

import java.io.File;
import java.util.List;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.ReportItems;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.CertificateView;

/**
 * Created by fernanda on 26/09/16.
 */

public interface CertificatePresenter extends BasePresenter{

     void loadData(String userId,String idFarm);
    void SuccesloadData(Report report);
    void failloadData(String erro);
      void generatePdf(Report report);
     void onSucessPdfGenerated(File path);
     void onErrorPdfGenerated(String error);
     void obtainView(CertificateView certificateActivity);

    void isContained(List<ReportItems> itensCertificate);

    boolean removeAtualFarm(Context context);
}

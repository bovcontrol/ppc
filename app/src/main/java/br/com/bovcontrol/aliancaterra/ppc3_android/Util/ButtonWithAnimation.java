package br.com.bovcontrol.aliancaterra.ppc3_android.Util;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Button;
import java.lang.reflect.InvocationTargetException;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;

/**
 * Created by ricardoogliari on 12/31/15.
 */
public class ButtonWithAnimation extends Button {

    public String method;
    public int colorPressed;
    public int colorUnpressed;
    public boolean hasColorPressed;
    public boolean hasColorUnpressed;


    public ButtonWithAnimation(Context context) {
        super(context);
    }

    public ButtonWithAnimation(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.LinearLayoutWithAnimation,
                0, 0);

        setClickable(true);

        try {
            method = a.getString(R.styleable.LinearLayoutWithAnimation_ll_method);
            colorPressed = a.getInteger(R.styleable.LinearLayoutWithAnimation_ll_colorPressed, -1);
            colorUnpressed = a.getInteger(R.styleable.LinearLayoutWithAnimation_ll_colorUnpressed, -1);
            hasColorPressed = a.hasValue(R.styleable.LinearLayoutWithAnimation_ll_colorPressed);
            hasColorUnpressed = a.hasValue(R.styleable.LinearLayoutWithAnimation_ll_colorUnpressed);
        } finally {
            a.recycle();
        }
    }

    public ButtonWithAnimation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN && isEnabled()) {
            int colorFrom = hasColorUnpressed ? colorUnpressed : getResources().getColor(R.color.colorBeje);
            int colorTo = hasColorPressed ? colorPressed : getResources().getColor(R.color.colorBeigeDark);
            int duration = 300;
            ObjectAnimator.ofObject(this, "backgroundColor", new ArgbEvaluator(), colorFrom, colorTo)
                    .setDuration(duration)
                    .start();
            return true;
        } else if (event.getAction() == MotionEvent.ACTION_CANCEL && isEnabled()) {
            int colorFrom = hasColorPressed ? colorPressed : getResources().getColor(R.color.colorBeigeDark);
            int colorTo = hasColorUnpressed ? colorUnpressed : getResources().getColor(R.color.colorBeje);
            int duration = 300;
            ObjectAnimator animator = ObjectAnimator.ofObject(this, "backgroundColor", new ArgbEvaluator(), colorFrom, colorTo);
            animator.setDuration(duration);
            animator.start();
            return true;
        } else if (event.getAction() == MotionEvent.ACTION_UP && isEnabled()) {
            int colorFrom = hasColorPressed ? colorPressed : getResources().getColor(R.color.colorBeigeDark);
            int colorTo = hasColorUnpressed ? colorUnpressed : getResources().getColor(R.color.colorBeje);
            int duration = 300;
            ObjectAnimator animator = ObjectAnimator.ofObject(this, "backgroundColor", new ArgbEvaluator(), colorFrom, colorTo);
            animator.setDuration(duration);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    //next(null);
                    Object obj=null;
                    if (method != null) {
                        try {
                            getContext().getClass().getMethod(method, new Class[]{}).invoke(getContext(),obj);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        }
                    }
                    performClick();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            animator.start();
            return true;
        }
        return false;
    }
}

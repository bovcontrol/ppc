package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.ValidaCpfCnpj;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Partner;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.fragments.ReportFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailFarm extends BaseActivity {

    @BindView(R.id.btnContinuar)
    Button btnContinuar;
    @BindView(R.id.edtDetailCpfCnpj)
    public EditText edtDetailCpfCnpj;

    @BindView(R.id.edtDetailFarm)
    public TextInputEditText edtDetailFarm;


    @BindView(R.id.inputLayoutDetailFarm)
    TextInputLayout inputLayoutDetailFarm;
    @BindView(R.id.inputLayoutDetailCpfCnpj)
    TextInputLayout inputLayoutDetailCpfCnpj;

    boolean added = false;
    private static final String maskCNPJ = "##.###.###/####-##";
    private static final String maskCPF = "###.###.###-##";
    @BindView(R.id.havePartner)
    AppCompatCheckBox havePartner;
    @BindView(R.id.addmorefields)
    LinearLayout addmorefields;
    @BindView(R.id.edtAddressFarm)
    TextInputEditText edtAddressFarm;

    private boolean isUpdating;
    String old = "";

    private String idUser;
    boolean hasPartner = false;
    LinearLayout lladdmorefields;
    List<LinearLayout> listLinearLayout = new ArrayList<>();
    private boolean isEdited = false;

    public static String unmask(String s) {
        return s.replaceAll("[^0-9]*", "");
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_farm);
        idUser = SharedPreferenceManager.getId(this);
        ButterKnife.bind(this);
        addValidation(edtDetailCpfCnpj, edtDetailFarm, inputLayoutDetailCpfCnpj, inputLayoutDetailFarm);
        Bundle extras = getIntent().getExtras();
        String report;

        if (extras != null && extras.getString(ReportFragment.Report) != null) {
            createToolbarBackArrowFinish();
        } else {
            createToolbar();
        }

        if (SharedPreferenceManager.getBoolean(this, "from_choice")) {
            SharedPreferenceManager.saveBoolean(this, "from_choice", false);
            isEdited = true;
            setUsersValues(FirebaseManager.getFarm(this));
        } else {
            StatesOfFlowApplication states = SharedPreferenceManager.getState(this);
            if (states != null) {
                switch (states) {
                    case INFOS_ABOUT_FARM:
                        Intent intent2 = new Intent(this, ChoiceAboutLocationActivity.class);
                        startActivity(intent2);
                        return;
                }
            }
        }


        havePartner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    lladdmorefields = addFields(true);
                    addmorefields.addView(lladdmorefields);
                    listLinearLayout.add(lladdmorefields);
                } else {
                    listLinearLayout = new ArrayList<LinearLayout>();
                    addmorefields.removeAllViews();
                }
            }
        });
    }

    private void setUsersValues(br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm detailFarm) {
        edtDetailFarm.setText(detailFarm.nameFarm);
        edtAddressFarm.setText(detailFarm.address);
        edtDetailCpfCnpj.setText(detailFarm.cpf_cnpj);
        if (detailFarm.partners.entrySet().size() != 0) {
            havePartner.setChecked(true);
            listLinearLayout = new ArrayList<LinearLayout>();
            addmorefields.removeAllViews();
        }

        int i = 1;
        int size = detailFarm.partners.entrySet().size();

        for (Map.Entry<String, Partner> partners : detailFarm.partners.entrySet()) {
            LinearLayout[] llchild = new LinearLayout[1];
            LinearLayout ll = addFields(false);
            EditText edCnpjCpf = (EditText) ll.findViewById(R.id.edtPartnerCpfCnpj);
            TextInputEditText edPartnerName = (TextInputEditText) ll.findViewById(R.id.edtPartnerName);
            edCnpjCpf.setText(partners.getValue().cpf_cnpj);
            edPartnerName.setText(partners.getValue().name);
            AppCompatCheckBox checkbox = (AppCompatCheckBox) ll.findViewById(R.id.addmorepartner);
            if (size != i) {
                checkbox.setChecked(true);
            }
            setBehaviorCheckBox(llchild, checkbox);
            addmorefields.addView(ll);
            listLinearLayout.add(ll);
            i++;
        }

        setCheckDisabled();
    }


    private LinearLayout addFields(boolean addBehavior) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.add_partner, null);
        final LinearLayout[] llchild = new LinearLayout[1];
        final AppCompatCheckBox checkbox = (AppCompatCheckBox) ll.findViewById(R.id.addmorepartner);
        if (addBehavior)
            setBehaviorCheckBox(llchild, checkbox);

        EditText edCnpjCpf = (EditText) ll.findViewById(R.id.edtPartnerCpfCnpj);
        TextInputEditText edPartnerName = (TextInputEditText) ll.findViewById(R.id.edtPartnerName);
        TextInputLayout tilCnpjCpf = (TextInputLayout) ll.findViewById(R.id.inputLayoutPartnerCpfCnpj);
        TextInputLayout tilPartnerName = (TextInputLayout) ll.findViewById(R.id.inputLayoutPartnerName);
        addValidation(edCnpjCpf, edPartnerName, tilCnpjCpf, tilPartnerName);
        return ll;
    }

    private void setBehaviorCheckBox(final LinearLayout[] llchild, AppCompatCheckBox checkbox) {
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    llchild[0] = addFields(true);
                    addmorefields.addView(llchild[0]);
                    listLinearLayout.add(llchild[0]);
                    setCheckDisabled();

                } else {
//                    LinearLayout llchildToRemove= getLinearLayout(buttonView);
                    int position = listLinearLayout.indexOf(buttonView.getParent());
                    listLinearLayout.remove(position + 1);
                    addmorefields.removeViewAt(position + 1);
                    setCheckDisabled();
                }
            }
        });
    }

    private void setCheckDisabled() {
        for (LinearLayout ll : listLinearLayout) {
            setCheckBoxEnable(ll, false);
        }

        if (listLinearLayout.size() - 2 >= 0) {
            LinearLayout last = listLinearLayout.get(listLinearLayout.size() - 2);
            setCheckBoxEnable(last, true);
            LinearLayout ll = listLinearLayout.get(listLinearLayout.size() - 1);
            setCheckBoxEnable(ll, true);
        }
        if (listLinearLayout.size() - 1 >= 0) {
            LinearLayout ll = listLinearLayout.get(listLinearLayout.size() - 1);
            setCheckBoxEnable(ll, true);
        }
        if (listLinearLayout.size() - 1 == 0)
            havePartner.setEnabled(true);
        else
            havePartner.setEnabled(false);

    }

    private void setCheckBoxEnable(LinearLayout e, boolean enabled) {
        if (e.findViewById(R.id.addmorepartner) != null)
            e.findViewById(R.id.addmorepartner).setEnabled(enabled);
        else
            e.findViewById(R.id.havePartner).setEnabled(enabled);
    }

    private LinearLayout getLinearLayout(CompoundButton buttonView) {
        return ((LinearLayout) buttonView.getParent());
    }

    private void addValidation(final EditText editTextCpfCnpj, EditText name, final TextInputLayout inputLayoutDetailCpfCnpj, final TextInputLayout inputLayoutname) {
        editTextCpfCnpj.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = unmask(s.toString());
                String mask;
                String defaultMask = getDefaultMask(str);
                switch (str.length()) {
                    case 11:
                        mask = maskCPF;
                        break;
                    case 14:
                        mask = maskCNPJ;
                        break;

                    default:
                        mask = defaultMask;
                        break;
                }

                String mascara = "";
                if (isUpdating) {
                    old = str;
                    isUpdating = false;
                    return;
                }
                int i = 0;
                for (char m : mask.toCharArray()) {
                    if ((m != '#' && str.length() > old.length()) || (m != '#' && str.length() < old.length() && str.length() != i)) {
                        mascara += m;
                        continue;
                    }

                    try {
                        mascara += str.charAt(i);
                    } catch (Exception e) {
                        break;
                    }
                    i++;
                }
                isUpdating = true;
                editTextCpfCnpj.setText(mascara);
                editTextCpfCnpj.setSelection(mascara.length());

                inputLayoutDetailCpfCnpj.setErrorEnabled(false);
                inputLayoutDetailCpfCnpj.setError(null);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {

            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutname.setErrorEnabled(false);
                inputLayoutname.setError(null);
            }
        });

//        edtDetailSocio.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                inputLayoutDetailSocio.setErrorEnabled(false);
//                inputLayoutDetailSocio.setError(null);
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.only_logoff, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        if (item.getItemId() == R.id.miLogoff) {
            logoff();
            return true;
        } else if (item.getItemId() == R.id.miMoreApps) {
            openBovcontrolOnboarding();

            return true;
        } else {
            return false;
        }
    }

    private static String getDefaultMask(String str) {
        String defaultMask = maskCPF;
        if (str.length() > 11) {
            defaultMask = maskCNPJ;
        }
        return defaultMask;
    }

    @OnClick(R.id.btnContinuar)
    public void continuar(View view) {
        boolean isValid = true;
        br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm detail = new br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm();

        if (listLinearLayout != null && listLinearLayout.size() != 0) {
            HashMap<String, String> cpfcnpj = new HashMap<>();

            for (LinearLayout ll : listLinearLayout) {
                EditText edCnpjCpf = (EditText) ll.findViewById(R.id.edtPartnerCpfCnpj);
                EditText edPartnerName = (EditText) ll.findViewById(R.id.edtPartnerName);
                TextInputLayout tilPartnerCnpjCpf = (TextInputLayout) ll.findViewById(R.id.inputLayoutPartnerCpfCnpj);
                TextInputLayout tilPartnerName = (TextInputLayout) ll.findViewById(R.id.inputLayoutPartnerName);
                if (validateFields(edCnpjCpf.getText().toString(), edPartnerName.getText().toString(), tilPartnerCnpjCpf, tilPartnerName) && isnottheSameOwner(edCnpjCpf.getText().toString(), tilPartnerCnpjCpf) && notHaveEqualCnpjCpf(edCnpjCpf.getText().toString(), cpfcnpj, tilPartnerCnpjCpf)) {
                    cpfcnpj.put(edCnpjCpf.getText().toString(), edCnpjCpf.getText().toString());
                    detail.addPartner(edCnpjCpf.getText().toString(), edPartnerName.getText().toString());
                } else
                    isValid = false;
            }
        }
        detail.cpf_cnpj = edtDetailCpfCnpj.getText().toString();
        detail.nameFarm = edtDetailFarm.getText().toString();
        detail.address = edtAddressFarm.getText().toString();
//        detail.socio = edtDetailSocio.getText().toString();

        if (validateFields(detail.cpf_cnpj, detail.nameFarm, inputLayoutDetailCpfCnpj, inputLayoutDetailFarm) && isValid) {
            FirebaseManager.saveDetailFarm(detail, idUser, new FirebaseListener() {
                @Override
                public void successAction() {
                    mobileAnalytics.AddEvent(DetailFarm.this,DetailFarm.this,"savedatailfarm.sucess");
                    SharedPreferenceManager.saveState(DetailFarm.this, StatesOfFlowApplication.INFOS_ABOUT_FARM);
                    FirebaseManager.sessionUserSave(DetailFarm.this, SharedPreferenceManager.getId(DetailFarm.this), StatesOfFlowApplication.INFOS_ABOUT_FARM.getName());
                    Intent intent = new Intent(DetailFarm.this, ChoiceAboutLocationActivity.class);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void errorAction() {
                    mobileAnalytics.AddEvent(DetailFarm.this,DetailFarm.this,"savedatailfarm.fail");
                }
            }, DetailFarm.this, isEdited);
        }
    }

    private boolean isnottheSameOwner(String cnpjcpf, TextInputLayout textcnpjcpf) {
        if (TextUtils.isEmpty(edtDetailCpfCnpj.getText().toString())) {
            return true;
        } else {
            if (edtDetailCpfCnpj.getText().toString().equals(cnpjcpf)) {
                textcnpjcpf.setError(getString(R.string.error_partner_cpf_cnpf_equals_owner));
                return false;
            }
            return true;
        }

    }

    private boolean notHaveEqualCnpjCpf(String cnpjcpf, HashMap<String, String> hashmapcnpjcpf, TextInputLayout textcnpjcpf) {
        if (hashmapcnpjcpf.get(cnpjcpf) != null) {
            textcnpjcpf.setError(getString(R.string.error_partner_with_same_cnpj));
            return false;
        }
        return true;
    }

    public boolean validateFields(String cpf_cnpj, String name, TextInputLayout inputLayoutDetailCpfCnpj, TextInputLayout inputLayoutDetailFarm) {
        boolean retorno = true;

        String cpf_cnpj_unmask = unmask(cpf_cnpj.trim());
        if (cpf_cnpj_unmask.length() <= 11) {
            if (!ValidaCpfCnpj.isValidCPF(cpf_cnpj_unmask)) {
                inputLayoutDetailCpfCnpj.setError(getString(R.string.invalid_cpf));
                retorno = false;
            }
        } else {
            if (!ValidaCpfCnpj.isValidCNPJ(cpf_cnpj_unmask)) {
                inputLayoutDetailCpfCnpj.setError(getString(R.string.invalid_cnpj));
                retorno = false;
            }
        }

        if (name.trim().length() < 3) {
            inputLayoutDetailFarm.setError(getString(R.string.farm_3_characteres));
            retorno = false;
        }

//        if (socio.trim().length() < 3){
//            inputLayoutDetailSocio.setError(getString(R.string.patner_3_characteres));
//            retorno = false;
//        }

        return retorno;
    }

}

package br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.UserInformations;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.LatLng;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.ReportItems;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.FarmRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;

/**
 * Created by fernanda on 27/09/16.
 */

public class FarmRepositoryImpl implements FarmRepository {


    @Override
    public UserInformations getFarmInformations() {
        return null;
    }


    @Override
    public LatLng getLatLng(Context context) {

        DetailFarm detailFarm = FirebaseManager.getFarm(context);
        if(detailFarm!=null){
            String point=detailFarm.point;
            if(!TextUtils.isEmpty(point)){
                String[] split=point.split(",");
                LatLng latLng= new LatLng(Double.parseDouble(split[1]),Double.parseDouble(split[0]));
                return latLng;

            }
        }
        return new LatLng(0,0);
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.FarmPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.FarmPresenterImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.FarmView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class MapFragment extends BaseFragment implements OnMapReadyCallback,FarmView {

    SupportMapFragment fragment;
    GoogleMap gMap = null;
    FarmPresenter presenter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        presenter=new FarmPresenterImpl(this,getActivity());
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        fragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (fragment == null) {
            fragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, fragment).commit();
        }
    }


        @Override
        public void onResume() {
            super.onResume();
            if (gMap == null) {
                fragment.getMapAsync(this);

            }else{
                presenter.getLtnLng();
            }
        }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {
                if (gMap != null && presenter!=null) {
                    presenter.getLtnLng();
                }
            }
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap=googleMap;
        presenter.getLtnLng();
        gMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        gMap.getUiSettings().setMyLocationButtonEnabled(true);
        gMap.getUiSettings().setCompassEnabled(true);
        gMap.getUiSettings().setRotateGesturesEnabled(true);
        gMap.getUiSettings().setMapToolbarEnabled(true);

    }

    @Override
    public void setMarker(Double lat, Double Lng) {
        gMap.clear();
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, Lng)).zoom(12).build();
        gMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        // create marker
        MarkerOptions marker = new MarkerOptions().position(new LatLng(
               lat, Lng));
        // adding marker
        gMap.addMarker(marker);
    }
}

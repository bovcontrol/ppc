package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import java.io.File;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by fernanda on 30/09/16.
 */
@RuntimePermissions
public abstract class GenerateCertificate extends BaseActivity {

    abstract void writebypresenter();
    abstract  String getActivityName();
    public Report report;

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void writeFile() {
        writebypresenter();
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showRationaleForWrite(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_camera_rationale)
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        writebypresenter();

                        request.proceed();

                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForCamera() {
        Toast.makeText(this, R.string.permission_camera_denied, Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showNeverAskForCamera() {
        Toast.makeText(this, R.string.permission_camera_neverask, Toast.LENGTH_SHORT).show();
    }

    public void sucessPdfGenerator(File file) {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri contentUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", file);
        sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        sharingIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        sharingIntent.setData(contentUri);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
        sharingIntent.setType("application/pdf");
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.sharemsg)));
    }

    public void onErrorPdfGenerated() {
        Toast.makeText(this,R.string.erro_generic,Toast.LENGTH_LONG).show();
    }

    public void clickToGenerateReport(){
        if(report!=null && report.paid!=null && report.paid.equals("true")) {
            mobileAnalytics.AddEvent(this,this,"sharereport");
            SharedPreferenceManager.saveState(this, StatesOfFlowApplication.REPORT);
            SessionManager.saveToPrefs(this,SessionManager.PREFS_ACTIVITY_SHOW_REPORT,getActivityName());
            GenerateCertificatePermissionsDispatcher.writeFileWithCheck(this);
        }else{
            Intent intent= new Intent(this,DoYouHaveCupomActivity.class);
            startActivity(intent);
        }
    }

    void verifyExtra() {

        Bundle extras = getIntent().getExtras();
        String generate;

        if (extras != null) {
            generate= extras.getString("GenerateCertificate");
            if("yes".equals(generate))
                writebypresenter();

        }

    }
}

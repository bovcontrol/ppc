package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.DeepLinkHelper;


public class DeepLinkOpenActivitiesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deep_link_open_activities);
        if(DeepLinkHelper.verifyDeepLinking(getIntent().getData())){
            DeepLinkHelper.openActivity(getIntent().getData(),this);
        }
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.Button;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StatusBarUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PresentationActivity extends BaseActivity {

    @BindView(R.id.btnContinuar)
    Button btnContinuar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presentation);
        ButterKnife.bind(this);
        boolean isUserFirstTime = Boolean.valueOf(SessionManager.getFromPrefs(this, SessionManager.PREF_USER_FIRST_TIME, "true"));
        Intent introIntent = new Intent(this, EnterWithActivity.class);
        introIntent.putExtra(SessionManager.PREF_USER_FIRST_TIME, isUserFirstTime);
        if (!isUserFirstTime)
            startActivity(introIntent);
        Window window = getWindow();
        StatusBarUtil.changeStatusBar(window,getResources().getColor(R.color.colorPrimaryDark));

    }

    @OnClick(R.id.btnContinuar)
    public void onClick() {
        mobileAnalytics.AddEvent(this,this,"button.presentation.next");
        Intent intent= new Intent(this,OnBoardingActivity.class);
        startActivity(intent);
//
        finish();
    }
}

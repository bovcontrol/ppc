package br.com.bovcontrol.aliancaterra.ppc3_android.Util;

import android.content.Context;
import android.util.Log;

/**
 * Created by fernanda on 12/12/16.
 */

public class Resources {

    private android.content.res.Resources resource;
    private Context mcontext;

    public Resources(Context context) {
        mcontext=context;
        resource=context.getResources();
    }
    public String getLabelInResource(String label) {
        try{
            int labelresource=resource.
                    getIdentifier(label, "string", mcontext.getPackageName());
            if(labelresource!=0 && labelresource!=-1 )
                label=resource.getString(labelresource);
            return label;
        }catch(Exception e){
            return "";
        }
    }
}

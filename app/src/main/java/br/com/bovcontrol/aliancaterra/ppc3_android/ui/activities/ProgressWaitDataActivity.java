package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.GsonHelper;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;

/**
 * Created by nic on 04/10/16.
 */

public class ProgressWaitDataActivity extends AppCompatActivity {

    @Override
    public void onBackPressed() {
//        super.onBackPressed();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_wait_data);

        String idUser = SharedPreferenceManager.getId(this);
        String idFarm = SessionManager.getFromPrefs(this,SessionManager.PREFS_FARM_UNIQUE_ID,"");
        final List<String> stringList= new ArrayList<>();
        MainApplication.myRef.child("reports").child(idUser).child(idFarm).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                GsonHelper<Report> gsonHelper=new GsonHelper();
                String reportJson=SessionManager.getFromPrefs(ProgressWaitDataActivity.this,SessionManager.REPORT_INFORMATION,"");
                Report report=new Report();
                if(!TextUtils.isEmpty(reportJson)){
                   report=gsonHelper.fromJson(reportJson,Report.class);
                }
                report.add(dataSnapshot.getKey(),dataSnapshot.getValue().toString());
                stringList.add(dataSnapshot.getKey());
                String json=gsonHelper.toJson(report);
                if(stringList.size()==5) {
                    Intent intent = new Intent(ProgressWaitDataActivity.this, CertificateActivity.class);
                    startActivity(intent);
                    SessionManager.saveToPrefs(ProgressWaitDataActivity.this,SessionManager.REPORT_INFORMATION,json);
                    finish();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}

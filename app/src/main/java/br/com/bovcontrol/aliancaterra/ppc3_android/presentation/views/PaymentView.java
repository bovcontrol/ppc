package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views;

/**
 * Created by fernanda on 26/10/16.
 */

public interface PaymentView {
    public void showLoading();
    public  void hideLoading();
    public void openWebView(String token);
    public void errorTogetToken();

    void sucessToSave();
}

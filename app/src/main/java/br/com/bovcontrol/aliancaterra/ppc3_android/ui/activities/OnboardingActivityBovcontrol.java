package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.MobileAnalytics;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StatusBarUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class OnboardingActivityBovcontrol extends AppCompatActivity {

    @BindView(R.id.intro_indicator_0)
    ImageView introIndicator0;
    @BindView(R.id.intro_btn_skip)
    Button introBtnSkip;
    @BindView(R.id.intro_btn_next)
    Button introBtnNext;
    @BindView(R.id.intro_btn_finish)
    Button introBtnFinish;
    @BindView(R.id.container)
    ViewPager container;
    @BindView(R.id.intro_indicator_1)
    ImageView introIndicator1;
    @BindView(R.id.intro_indicator_2)
    ImageView introIndicator2;
    @BindView(R.id.intro_indicator_3)
    ImageView introIndicator3;

    ImageView[] indicators;
    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private OnboardingActivityBovcontrol.SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding_bovcontrol);
        ButterKnife.bind(this);
        MobileAnalytics mobileAnalytics= new MobileAnalytics();
        mobileAnalytics.AddEvent(this,this,"bovcontrolOnboarding");
        if(!TextUtils.isEmpty(SessionManager.getFromPrefs(this,SessionManager.PREFS_BovControlONBOARDING,""))){
            showBovControl();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new OnboardingActivityBovcontrol.SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        indicators = new ImageView[]{introIndicator0, introIndicator1, introIndicator2, introIndicator3};
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {
                page = position;
                updateIndicators(page);
                introBtnNext.setVisibility(position == 3 ? View.INVISIBLE : View.VISIBLE);
                introBtnSkip.setVisibility(position == 3 ? View.INVISIBLE : View.VISIBLE);
                introBtnFinish.setVisibility(position == 3 ? View.VISIBLE : View.INVISIBLE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        Window window = getWindow();
        StatusBarUtil.changeStatusBar(window,getResources().getColor(R.color.gray));


    }

    private void showBovControl() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://web.bovcontrol.com/app"));
        startActivity(browserIntent);
        finish();
    }

    void updateIndicators(int position) {
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected_white : R.drawable.indicator_unselected_white
            );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_on_boarding, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.intro_btn_skip, R.id.intro_btn_next, R.id.intro_btn_finish})
    public void onClick(View view) {
//        Toast.makeText(OnBoardingActivity.this,"CLICKOU",Toast.LENGTH_LONG).show();
        switch (view.getId()) {

            case R.id.intro_btn_next:
                page += 1;
                mViewPager.setCurrentItem(page, true);

                break;
            case R.id.intro_btn_skip:
            case R.id.intro_btn_finish:
                SessionManager.saveToPrefs(this,SessionManager.PREFS_BovControlONBOARDING,"true");
               showBovControl();
//                String appPackageName = "com.bovcontrol.bovcontrol";
//                try {
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                } catch (android.content.ActivityNotFoundException anfe) {
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
//                }

                finish();
                break;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        int[] icons = new int[]{R.drawable.bovcontrol_slide_1, R.drawable.cows_slide2,R.drawable.vacine_cow, R.drawable.finance, R.drawable.bovcontrol_final};
        int[] titles = new int[]{R.string.onboardingbovcontrol_title_slide_1, R.string.onboardingbovcontrol_title_slide_2, R.string.onboardingbovcontrol_title_slide_3, R.string.onboardingbovcontrol_title_slide_4,R.string.onboardingbovcontrol_title_slide_5};
        int[] descriptions = new int[]{R.string.onboardingbovcontrol_msg_slide_1, R.string.onboardingbovcontrol_msg_slide_2, R.string.onboardingbovcontrol_msg_slide_3, R.string.onboardingbovcontrol_msg_slide_4,R.string.onboardingbovcontrol_msg_slide_5};


        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static OnboardingActivityBovcontrol.PlaceholderFragment newInstance(int sectionNumber) {
            OnboardingActivityBovcontrol.PlaceholderFragment fragment = new OnboardingActivityBovcontrol.PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_on_boarding_bovcontrol, container, false);
            TextView title = (TextView) rootView.findViewById(R.id.title_onboarding);
            TextView description = (TextView) rootView.findViewById(R.id.description_onboarding);
            ImageView imageView = (ImageView) rootView.findViewById(R.id.icon_onboarding);
//            bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1])
            title.setText(getString(titles[getArguments().getInt(ARG_SECTION_NUMBER) - 1]));
            description.setText(getString(descriptions[getArguments().getInt(ARG_SECTION_NUMBER) - 1]));
            imageView.setImageResource(icons[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            return rootView;
        }
    }


    public Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap will now
        // be
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return OnboardingActivityBovcontrol.PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 3";
            }
            return null;
        }
    }

}

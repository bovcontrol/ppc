package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters;

import android.content.Context;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;

/**
 * Created by fernanda on 24/10/16.
 */

public interface CupomPresenter {
    public void validateCupom(final String cupom, final Context context,final String idUser, final String farmId);

}

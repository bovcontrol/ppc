package br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import br.com.bovcontrol.aliancaterra.ppc3_android.Util.GsonHelper;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.UserRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;

/**
 * Created by fernanda on 27/09/16.
 */

public class UserRepositoryImpl implements UserRepository {


    @Override
    public boolean saveIntentiontoAccessNewFeatures(String feature,String userId) {
        try {
            FirebaseManager.saveClickRegister(feature, userId);
        }catch(Exception e){
            return false;
        }

        return true;
    }

    @Override
    public void sendEmailRecoverPassword(String Email, final FirebaseListener listener) {

        FirebaseAuth auth = FirebaseAuth.getInstance();
        String emailAddress = Email;

        auth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            listener.successAction();
                        }else{
                            listener.errorAction();
                        }
                    }
                });
    }

    @Override
    public boolean updatePasword(String password) {
        return true;
    }


    public void setInSessionUserInformation(final Context context, String userId) {
        String user=SessionManager.getFromPrefs(context,SessionManager.PREFS_USER_INFORMATION,"");
        if(TextUtils.isEmpty(user)){
            final Query query= MainApplication.myRef.child("users").child(userId);
            query.addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            GsonHelper<User> gsonHelper=new GsonHelper();
                            String json=gsonHelper.toJson(dataSnapshot.getValue(User.class));
                            SessionManager.saveToPrefs(context,SessionManager.PREFS_USER_INFORMATION,json);
                            query.removeEventListener(this);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.w("ERROO", "getUser:onCancelled", databaseError.toException());

                            query.removeEventListener(this);
                        }
                    });
        }

    }
}

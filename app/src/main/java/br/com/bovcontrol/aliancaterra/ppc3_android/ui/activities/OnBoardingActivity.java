package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StatusBarUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OnBoardingActivity extends AppCompatActivity {

    @BindView(R.id.intro_indicator_0)
    ImageView introIndicator0;
    @BindView(R.id.intro_btn_skip)
    Button introBtnSkip;
    @BindView(R.id.intro_btn_next)
    Button introBtnNext;
    @BindView(R.id.intro_btn_finish)
    Button introBtnFinish;
    @BindView(R.id.container)
    ViewPager container;
    @BindView(R.id.intro_indicator_1)
    ImageView introIndicator1;
    @BindView(R.id.intro_indicator_2)
    ImageView introIndicator2;
    @BindView(R.id.intro_indicator_3)
    ImageView introIndicator3;

    ImageView[] indicators;
    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        indicators = new ImageView[]{introIndicator0, introIndicator1, introIndicator2, introIndicator3};
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {
                page = position;
                updateIndicators(page);
                introBtnNext.setVisibility(position == 3 ? View.GONE : View.VISIBLE);
                introBtnSkip.setVisibility(position == 3 ? View.GONE : View.VISIBLE);
                introBtnFinish.setVisibility(position == 3 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        Window window = getWindow();
        StatusBarUtil.changeStatusBar(window,getResources().getColor(R.color.gray));


    }

    void updateIndicators(int position) {
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_on_boarding, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.intro_btn_skip, R.id.intro_btn_next, R.id.intro_btn_finish})
    public void onClick(View view) {
//        Toast.makeText(OnBoardingActivity.this,"CLICKOU",Toast.LENGTH_LONG).show();
        switch (view.getId()) {

            case R.id.intro_btn_next:
                page += 1;
                mViewPager.setCurrentItem(page, true);

                break;
            case R.id.intro_btn_skip:
            case R.id.intro_btn_finish:
                Intent intent= new Intent(OnBoardingActivity.this,EnterWithActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        int[] icons = new int[]{R.drawable.createuser, R.drawable.createfarm, R.drawable.createreport, R.drawable.results};
        int[] titles = new int[]{R.string.onboarding_createuser_title, R.string.onboarding_createfarm_title, R.string.onboarding_createreport_title, R.string.onboarding_result_title};
        int[] descriptions = new int[]{R.string.onboarding_createuser_description, R.string.onboarding_createfarm_description, R.string.onboarding_createreport_description, R.string.onboarding_result_description};


        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_on_boarding, container, false);
            TextView title = (TextView) rootView.findViewById(R.id.title_onboarding);
            TextView description = (TextView) rootView.findViewById(R.id.description_onboarding);
            ImageView imageView = (ImageView) rootView.findViewById(R.id.icon_onboarding);
//            bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1])
            title.setText(getString(titles[getArguments().getInt(ARG_SECTION_NUMBER) - 1]));
            description.setText(getString(descriptions[getArguments().getInt(ARG_SECTION_NUMBER) - 1]));
            imageView.setImageDrawable(getResources().getDrawable(icons[getArguments().getInt(ARG_SECTION_NUMBER) - 1]));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 3";
            }
            return null;
        }
    }
}

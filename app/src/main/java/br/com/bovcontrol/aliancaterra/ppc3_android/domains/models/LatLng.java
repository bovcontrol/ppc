package br.com.bovcontrol.aliancaterra.ppc3_android.domains.models;

/**
 * Created by fernanda on 30/09/16.
 */

public class LatLng {
    private double Lng;
    private double lat;

    public LatLng(double lat, double lng) {
        this.Lng = lng;
        this.lat = lat;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return Lng;
    }

    public void setLng(double lng) {
        Lng = lng;
    }


}

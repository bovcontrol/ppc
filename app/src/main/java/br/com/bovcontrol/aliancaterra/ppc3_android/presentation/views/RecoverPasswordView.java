package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views;

/**
 * Created by fernanda on 14/11/16.
 */

public interface RecoverPasswordView {
     void SendEmailToRecover();
     void sucessTorecover();
     void failToRecover();
}

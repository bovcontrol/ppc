package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bovcontrol.signon.widget.BovLoginButton;
import com.bovcontrol.signon.widget.Click.ClickCallback;
import com.bovcontrol.signon.widget.models.BovControlResponse;
import com.google.firebase.auth.FirebaseAuth;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterWithActivity extends AuthActivity {

    @BindView(R.id.BovLoginButton)
    com.bovcontrol.signon.widget.BovLoginButton BovLoginButton;
    @BindView(R.id.content_enter_with)
    LinearLayout contentEnterWith;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private boolean isclickedSignIn=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_with);
        SessionManager.saveToPrefs(this, SessionManager.PREF_USER_FIRST_TIME, "false");
        ButterKnife.bind(this);
        BovLoginButton.OnResume();
        BovLoginButton.setCallback(new ClickCallback() {
            @Override
            public void getAccountInformation(BovControlResponse bovControlResponse) {


                String farm_id=bovControlResponse.getFarm_id();
                if(!TextUtils.isEmpty(farm_id)) {
                    createUserWithEmailAndPassword(bovControlResponse.getEmail(), farm_id, bovControlResponse.getName());
                }else{
                    Toast.makeText(EnterWithActivity.this, getString(R.string.invalid_account), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BovLoginButton.onPause();

    }
    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isclickedSignIn) {
            BovLoginButton.OnResume();
            isclickedSignIn=false;
        }

    }

    @OnClick(R.id.btnFacebook)
    public void facebook(View view) {
        mobileAnalytics.AddEvent(this,this,"button.facebook");
        loginByFace();
    }

    @OnClick(R.id.btnGoogle)
    public void google(View view) {
        mobileAnalytics.AddEvent(this,this,"button.google");
        loginByGoogle();
    }

    @OnClick(R.id.btnEmail)
    public void email(View view) {
        mobileAnalytics.AddEvent(this,this,"button.enterwithemail");
        Intent intent = new Intent(this, EnterWithEmailPass.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtSignIn)
    public void signIn(View view) {
        mobileAnalytics.AddEvent(this,this,"button.signin");
        Intent intent = new Intent(this, SignActivity.class);
        startActivity(intent);
        BovLoginButton.onPause();
        isclickedSignIn=true;
    }


}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.MobileAnalytics;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.RecyclerViewItemClickCompat;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.*;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.SlaugtherhousesPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.SlaugtherHousesPresenterImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.SlaugtherHousesView;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.adapters.SlaughterHousesAdapter;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.DividerItemDecoration;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SlaughterHousesActivity extends BaseActivity implements RecyclerViewItemClickCompat.OnItemClickListener,SlaugtherHousesView{

    SlaughterHousesAdapter adapter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.sh_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.cl)
    CoordinatorLayout clSlaughterHouses;
    SlaugtherhousesPresenter presenter;
    ProgressDialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slaughter_houses);
        ButterKnife.bind(this);
        createToolbarBackArrow();
        br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm detailFarm=FirebaseManager.getFarm(this);
        if(!TextUtils.isEmpty(detailFarm.request_promocode)){
            Toast.makeText(this,getString(R.string.not_possible_genrate_coupon),Toast.LENGTH_LONG).show();
            Intent intent=new Intent(this,CupomActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
        presenter=new SlaugtherHousesPresenterImpl(this);
        presenter.getSlaughters();


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    private void setupRecyclerView(HashMap<String,SlaugtherHouses> slaugtherHouses) {
        final LinearLayoutManager manager = new LinearLayoutManager(this);
        Location location= new Location("farmLocation");
        location=SlaugtherHouses.getLocation(FirebaseManager.getFarm(this).point,location);
        adapter = new SlaughterHousesAdapter(slaugtherHouses, this,location);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(adapter);
        Drawable dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider_black);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        RecyclerViewItemClickCompat.addTo(mRecyclerView).setOnItemClickListener(this);
    }


    @Override
    public void onItemClicked(final RecyclerView recyclerView, final int position, View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.PPC3AlerStyleGreen);
        builder.setTitle(String.format(getString(R.string.alert_slaugtherHouses_title),adapter.getItemName(position)));
        builder.setMessage(getString(R.string.alert_slaugtherHouses_msg));
        builder.setPositiveButton(getString(R.string.request), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.save(adapter.getItemHash(position), SharedPreferenceManager.getId(SlaughterHousesActivity.this),SlaughterHousesActivity.this);
               mobileAnalytics.AddEvent(SlaughterHousesActivity.this,SlaughterHousesActivity.this,"cupomby"+adapter.getItemHash(position));

            }
        });
        builder.setNegativeButton(getString(R.string.cancel), null);
        builder.show();
    }

    @Override
    public void showList(HashMap<String,SlaugtherHouses> slaugtherHousesList) {
        if(slaugtherHousesList!=null){
            setupRecyclerView(slaugtherHousesList);
        }
    }

    @Override
    public void showLoading() {
        mDialog = new ProgressDialog(this);
        mDialog.setTitle(getString(R.string.please_wait));
        mDialog.setIndeterminate(false);
        mDialog.setCancelable(false);
        mDialog.show();
    }

    @Override
    public void hideLoading() {
        if ((mDialog != null) && mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    public void successtogenerateCoupon() {
        Snackbar.make(clSlaughterHouses,getString(R.string.success_sent_to_slaugther),Snackbar.LENGTH_LONG).show();
        Intent i= new Intent(this,CupomActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void failtogenerateCoupon() {
        Snackbar.make(clSlaughterHouses,getString(R.string.fail_sent_to_slaugther),Snackbar.LENGTH_LONG).show();
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import br.com.bovcontrol.aliancaterra.ppc3_android.Util.GsonHelper;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.ReportItems;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.ReportRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.CertificatePresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.CertificateView;

/**
 * Created by nic on 04/10/16.
 */

public class ReportRepositoryImpl implements ReportRepository {
    CertificatePresenter presenter;

    public ReportRepositoryImpl(CertificatePresenter presenter){
        this.presenter=presenter;
    }

    @Override
    public void getReport(String userId, String idFarm,Context context) {
        String json=SessionManager.getFromPrefs(context,SessionManager.REPORT_INFORMATION,"");
        if(TextUtils.isEmpty(json)) {
            searchIndatabase(userId, idFarm);
        }else{
            GsonHelper<Report> gsonHelper= new GsonHelper<>();
            Report report=gsonHelper.fromJson(json,Report.class);
            if(report.paid!=null && report.paid.equals("true")) {
                presenter.SuccesloadData(report);
            }else{
                searchIndatabase(userId,idFarm);
            }
        }


    }

    private void searchIndatabase(String userId, String idFarm) {
        final Query query = MainApplication.myRef.child("reports").child(userId).child(idFarm);
        query.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Report report = dataSnapshot.getValue(Report.class);
                        presenter.SuccesloadData(report);
                        query.removeEventListener(this);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("ERROO", "getUser:onCancelled", databaseError.toException());
                        presenter.failloadData("Unexpected Error");
                        query.removeEventListener(this);
                    }
                });
    }




}

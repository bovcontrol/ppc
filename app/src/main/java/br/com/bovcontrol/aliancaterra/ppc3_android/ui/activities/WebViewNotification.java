package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;


public class WebViewNotification extends AppCompatActivity {
    private WebView webView;
    String url;
    ProgressBar progress;
    public static String TAG_INTENT_EXTRA = "url";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_notification);
        this.progress = (ProgressBar)this.findViewById(R.id.progressBar);
        this.url = this.getIntent().getStringExtra(TAG_INTENT_EXTRA);
        this.openWebview();
    }
    public void openWebview() {
        this.webView = (WebView)this.findViewById(R.id.webview_notification);
        this.progress = (ProgressBar)this.findViewById(R.id.progressBar);
        this.webView.setVisibility(View.GONE);
        WebSettings webSettings = this.webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        this.webView.setWebChromeClient(new WebChromeClient() {
            public void onReceivedTitle(WebView view, String title) {
                WebViewNotification.this.getWindow().setTitle(title);
            }

            public void onProgressChanged(WebView view, int progressInt) {
                WebViewNotification.this.progress.setProgress(progressInt);
                if(progressInt == 100) {
                    WebViewNotification.this.progress.setVisibility(View.GONE);
                } else {
                    WebViewNotification.this.progress.setVisibility(View.VISIBLE);
                }

            }
        });
        this.webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                WebViewNotification.this.progress.setVisibility(View.GONE);
                WebViewNotification.this.webView.setVisibility(View.VISIBLE);
                super.onPageFinished(view, url);
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                WebViewNotification.this.progress.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
            }
        });
        this.webView.loadUrl(this.url);


    }

    public void setValue(int progress) {
        this.progress.setProgress(progress);
    }
}

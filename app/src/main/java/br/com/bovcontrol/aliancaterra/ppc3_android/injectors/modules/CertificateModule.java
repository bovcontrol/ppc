package br.com.bovcontrol.aliancaterra.ppc3_android.injectors.modules;

import javax.inject.Singleton;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.FarmRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl.FarmRepositoryImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.CertificatePresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.CertificatePresenterImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.CertificateView;
import dagger.Module;
import dagger.Provides;

/**
 * Created by fernanda on 27/09/16.
 */
@Module
public class CertificateModule {
    @Provides
    @Singleton
    public CertificatePresenter provideCertificateRepository() {
        return new CertificatePresenterImpl();
    }




}

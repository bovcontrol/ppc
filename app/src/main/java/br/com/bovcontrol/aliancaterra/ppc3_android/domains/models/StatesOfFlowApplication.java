package br.com.bovcontrol.aliancaterra.ppc3_android.domains.models;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.*;

/**
 * Created by ricardoogliari on 10/3/16.
 */

public enum StatesOfFlowApplication {

    LOGOFF("LOGOFF"),
    LOGGED("LOGGED"),
    INFOS_ABOUT_FARM("INFOS_ABOUT_FARM"),
    POINT_OF_FARM("POINT_OF_FARM"),
    CONFIRM("CONFIRM"),
    CUPOM("CUPOM"),
    REPORT("REPORT");

    private final String name;

    StatesOfFlowApplication(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static void showCurrentScreen(StatesOfFlowApplication state, Context context) {
         if (state != null) {
            switch (state) {
                case INFOS_ABOUT_FARM:
                case LOGGED:
                    Intent intent1 = new Intent(context, br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.DetailFarm.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent1);
                    break;
                case POINT_OF_FARM:
                    Intent intent3 = new Intent(context, ConfirmYourDataActivity.class);
                    intent3.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent3);
                    break;
                case CONFIRM:
                    Intent intent6 = new Intent(context, ProgressWaitDataActivity.class);
                    intent6.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent6);
                    break;
                case CUPOM:
                    Intent intent5 = new Intent(context, PostOrPayActivity.class);
                    intent5.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent5);
                    break;
                case REPORT:
                    Intent intent4 = new Intent(context, DashboardActivity.class);
                    intent4.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent4);
                    break;
                default:
                    Intent intentdefault = new Intent(context, br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.DetailFarm.class);
                    intentdefault.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intentdefault);
                    break;

            }

        }
    }
}

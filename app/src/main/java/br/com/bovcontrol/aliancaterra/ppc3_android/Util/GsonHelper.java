package br.com.bovcontrol.aliancaterra.ppc3_android.Util;

import com.google.gson.Gson;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.LatLng;

/**
 * Created by fernanda on 30/09/16.
 */

public class GsonHelper<T> {


    public String toJson(T latLng) {
        Gson gson= new Gson();
        String json=gson.toJson(latLng);
        return json;
    }

    public T fromJson(String fromPrefs,Class<T> classOfT) {
        Gson gson= new Gson();
        return gson.fromJson(fromPrefs,classOfT);
    }
}

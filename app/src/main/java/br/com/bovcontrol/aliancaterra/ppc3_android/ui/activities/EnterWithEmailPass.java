package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterWithEmailPass extends AuthActivity {

    @BindView(R.id.edtEnterEmailName)
    TextInputEditText edtName;
    @BindView(R.id.edtEnterEmailPass)
    TextInputEditText edtPass;
    @BindView(R.id.edtEnterEmailEmail)
    TextInputEditText edtEmail;

    @BindView(R.id.inputLayoutEnterEmail)
    TextInputLayout inputLayoutEnterEmail;
    @BindView(R.id.inputLayoutEnterEmailName)
    TextInputLayout inputLayoutEnterEmailName;
    @BindView(R.id.inputLayoutEnterEmailPass)
    TextInputLayout inputLayoutEnterEmailPass;
    @BindView(R.id.edtConfirmPass)
    TextInputEditText edtConfirmPass;
    @BindView(R.id.inputLayoutEnterEmailConfirmPass)
    TextInputLayout inputLayoutEnterEmailConfirmPass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_with_email_pass);
        ButterKnife.bind(this);

        createToolbarBackArrow();

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutEnterEmail.setErrorEnabled(false);
                inputLayoutEnterEmail.setError(null);
            }
        });

        edtPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutEnterEmailPass.setErrorEnabled(false);
                inputLayoutEnterEmailPass.setError(null);
            }
        });
        edtConfirmPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(edtPass.getText()!=null && edtConfirmPass.getText()!=null&&edtPass.getText().length()==edtConfirmPass.getText().length())
                    validConfirmPassword(edtPass.getText().toString(),true,edtConfirmPass.getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutEnterEmailConfirmPass.setErrorEnabled(false);
                inputLayoutEnterEmailConfirmPass.setError(null);
                setDrawableRight(edtConfirmPass,null);
            }
        });
    }

    @OnClick(R.id.btnCreateUser)
    public void createUser() {
        mobileAnalytics.AddEvent(this,this,"button.createuser");
        String nome = edtName.getText().toString();
        String email = edtEmail.getText().toString();
        String password = edtPass.getText().toString();

        if (validateFields(nome, email, password)) {
            createUserWithEmailAndPassword(email, password, nome);
        }
    }

    public boolean validateFields(String nome, String email, String password) {
        boolean retorno = true;
        String confirmpass=edtConfirmPass.getText().toString();
        if (password.trim().length() < 6) {
            inputLayoutEnterEmailPass.setError(getString(R.string.password_at_least));
            retorno = false;
        }

        retorno = validConfirmPassword(password, retorno, confirmpass);

        if (email.trim().length() < 1) {
            inputLayoutEnterEmail.setError(getString(R.string.email_must_filled));
            retorno = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            inputLayoutEnterEmail.setError(getString(R.string.incorrect_email));
            retorno = false;
        }


        return retorno;
    }

    private boolean validConfirmPassword(String password, boolean retorno, String confirmpass) {
        if (!password.equals(confirmpass)) {
            inputLayoutEnterEmailConfirmPass.setError(getString(R.string.confirmpassword_error));
            setDrawableRight(edtConfirmPass,getResources().getDrawable(R.drawable.ic_close_red_24dp));
            retorno = false;
        }else{
            setDrawableRight(edtConfirmPass,getResources().getDrawable(R.drawable.ic_done_white_24dp));
        }
        return retorno;
    }


    public void setDrawableRight(TextInputEditText ed,Drawable drawable){
        ed.setCompoundDrawables(null,null,drawable,null);
    }

}
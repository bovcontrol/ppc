package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views;

/**
 * Created by fernanda on 24/10/16.
 */

public interface CupomView extends BaseView {
    public void showCupomValide();
    public void showCupomInvalide();
}

package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters;

/**
 * Created by fernanda on 26/10/16.
 */

public interface PaymentPresenter {
    public void saveRequest_payment(String required);
    void successGetToken(String token);
    void errorGetToken(String error);
}

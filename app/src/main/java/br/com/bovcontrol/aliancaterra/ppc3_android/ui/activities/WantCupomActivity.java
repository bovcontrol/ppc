package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import java.util.HashMap;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.*;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.SlaugtherhousesPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.SlaugtherHousesPresenterImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.SlaugtherHousesView;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StatusBarUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class WantCupomActivity extends BaseActivity implements SlaugtherHousesView {

    @BindView(R.id.btnSelect)
    Button btnSelect;
    @BindView(R.id.btnNotSlaugtherHouses)
    Button btnNotSlaugtherHouses;
    SlaugtherhousesPresenter presenter;
    ProgressDialog mDialog;
    @BindView(R.id.clWantCUpom)
    CoordinatorLayout clWantCUpom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_want_cupom);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        createToolbarBackArrow();
        Window window = getWindow();
        StatusBarUtil.changeStatusBar(window, getResources().getColor(R.color.brownDark));
        br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm detailFarm = FirebaseManager.getFarm(this);
        if (!TextUtils.isEmpty(detailFarm.request_promocode)) {
            Toast.makeText(this, getString(R.string.not_possible_genrate_coupon), Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, CupomActivity.class);
            startActivity(intent);
            finish();
        }
        presenter = new SlaugtherHousesPresenterImpl(this);

    }

    @OnClick({R.id.btnSelect, R.id.btnNotSlaugtherHouses})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSelect:
                mobileAnalytics.AddEvent(this,this,"button.wantCupom.select");
                Intent intent = new Intent(this, SlaughterHousesActivity.class);
                startActivity(intent);
                break;
            case R.id.btnNotSlaugtherHouses:
                mobileAnalytics.AddEvent(this,this,"button.wantCupom.notslaugtherhouses");
                sendCoupon();
                break;
        }
    }

    private void sendCoupon() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.PPC3AlerStyleGreen);
        builder.setTitle(String.format(getString(R.string.alert_slaugtherHouses_title), getString(R.string.alianca_terra)));
        builder.setMessage(getString(R.string.alert_slaugtherHouses_msg));
        builder.setPositiveButton(getString(R.string.request), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showLoading();
                presenter.save("false", SharedPreferenceManager.getId(WantCupomActivity.this), WantCupomActivity.this);

            }
        });
        builder.setNegativeButton(getString(R.string.cancel), null);
        builder.show();
    }

    @Override
    public void showList(HashMap<String, SlaugtherHouses> slaugtherHousesList) {
        //Don't do anything
    }

    @Override
    public void showLoading() {
        mDialog = new ProgressDialog(this);
        mDialog.setTitle(getString(R.string.please_wait));
        mDialog.setIndeterminate(false);
        mDialog.setCancelable(false);
        mDialog.show();
    }

    @Override
    public void hideLoading() {
        if(mDialog!=null && mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    public void successtogenerateCoupon() {
        Snackbar.make(clWantCUpom, getString(R.string.success_sent_to_slaugther), Snackbar.LENGTH_LONG).show();
        Intent i = new Intent(this, CupomActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void failtogenerateCoupon() {
        Snackbar.make(clWantCUpom, getString(R.string.fail_sent_to_slaugther), Snackbar.LENGTH_LONG).show();
    }

}


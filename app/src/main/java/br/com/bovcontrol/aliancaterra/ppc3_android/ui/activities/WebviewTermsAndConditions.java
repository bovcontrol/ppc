package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;

public class WebviewTermsAndConditions extends BaseActivity {

    private WebView webView;
    ProgressBar progress;
    private static String URL = "https://docs.google.com/gview?url=https://goo.gl/Bn0cOF&embedded=true";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_terms_and_conditions);
        openWebview();
        createToolbarBackArrowFinish();
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    public void openWebview()
    {
        webView = (WebView) findViewById(R.id.webview_terms_and_conditions);
        webView.getSettings().setJavaScriptEnabled(true);
        progress = (ProgressBar)findViewById(R.id.progressBar);
        webView.setVisibility(View.GONE);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);

        webView.setWebChromeClient(new WebChromeClient(){

            public void onProgressChanged(WebView view, int progressInt){
                progress.setProgress(progressInt);
                if (progressInt == 100) {
                    progress.setVisibility(View.GONE);

                } else {
                    progress.setVisibility(View.VISIBLE);

                }
            }
        });
        //This is the section of code that fixes redirects to external apps
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                progress.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
                super.onPageFinished(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progress.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
            }
        });
        webView.loadUrl(URL);
    }
}

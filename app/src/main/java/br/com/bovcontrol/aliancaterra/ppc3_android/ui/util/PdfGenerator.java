package br.com.bovcontrol.aliancaterra.ppc3_android.ui.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Line;
import com.hendrix.pdfmyxml.PdfDocument;
import com.hendrix.pdfmyxml.viewRenderer.AbstractViewRenderer;

import java.util.Map;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.GsonHelper;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Partner;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.ReportItems;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.UserInformations;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;

/**
 * Created by fernanda on 26/09/16.
 */

public class PdfGenerator {

    Context mcontext;
    String nameFile="";
    private android.widget.TextView idLabel;
    private android.widget.TextView nameLabel;
    private android.widget.TextView emailLabel;
    private android.widget.TextView farmnameLabel;
    private android.widget.TextView cpfcnpjLabel;
    private android.widget.TextView farmAddressLabel;
    private LinearLayout llfarmAddress;
    private LinearLayout partners;
    private android.widget.TextView coordinatesLabel;
    private LinearLayout lldynamic_items;
    private android.widget.TextView dateGeneration;
    int countPartner=0;
    ResourcesUtil res=new ResourcesUtil();
    public PdfGenerator(Context context,String name) {
        mcontext = context;
        nameFile=name;
    }

    public PdfDocument generatePdf(AbstractViewRenderer page) {
        // you can reuse the bitmap if you want
//        page.setReuseBitmap(true);
        PdfDocument doc= new PdfDocument(mcontext);
// you can reuse the bitmap if you want
        page.setReuseBitmap(true);
        doc.addPage(page);
        doc.setSaveDirectory(FileUtils.getFileDirectory(mcontext));
        doc.setRenderWidth(1580);
        int minheight=3200;
        DetailFarm farm=FirebaseManager.getFarm(mcontext);
        countPartner=farm.partners.size();
        if(countPartner!=0)
            minheight+=(countPartner*120);
        Log.d("HEIGHT","minheight"+minheight);
        doc.setRenderHeight(4000);
        doc.setOrientation(PdfDocument.A4_MODE.PORTRAIT);
        doc.setProgressTitle(R.string.gen_please_wait);
        doc.setProgressMessage(R.string.gen_pdf_file);
        doc.setFileName(nameFile);
        doc.setInflateOnMainThread(false);

        return doc;
    }

    @NonNull
    public AbstractViewRenderer getAbstractViewRenderer(final Report report ) {
        return new AbstractViewRenderer(mcontext, R.layout.pagepdf) {
            @Override
            protected void initView(View view) {
                float pxBottomMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 16, mcontext.getResources().getDisplayMetrics());
                dateGeneration = (TextView) view.findViewById(R.id.dateGeneration);
                coordinatesLabel = (TextView) view.findViewById(R.id.coordinatesLabel);
                partners=(LinearLayout) view.findViewById(R.id.partners);
                cpfcnpjLabel = (TextView) view.findViewById(R.id.cpfcnpjLabel);
                farmnameLabel = (TextView) view.findViewById(R.id.farmnameLabel);
                farmAddressLabel = (TextView) view.findViewById(R.id.farmAddressLabel);
                llfarmAddress = (LinearLayout) view.findViewById(R.id.llfarmAddress);
                emailLabel = (TextView) view.findViewById(R.id.emailLabel);
                nameLabel = (TextView) view.findViewById(R.id.nameLabel);
                idLabel = (TextView) view.findViewById(R.id.idLabel);
                lldynamic_items=(LinearLayout) view.findViewById(R.id.lldynamic_items);
                LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                for(ReportItems items:report.getItensCertificate()){

                    LinearLayout dynamiLayout=(LinearLayout)inflater.inflate(R.layout.dynamic_item_pdf,null);
                    TextView tvatribute=(TextView) dynamiLayout.findViewById(R.id.atributepdf);
                    TextView tvValue=(TextView) dynamiLayout.findViewById(R.id.valuepdf);
                    TextView tvReason=(TextView) dynamiLayout.findViewById(R.id.reasonpdf);
                    TextView tvSource=(TextView) dynamiLayout.findViewById(R.id.sourcepdf);
                    tvatribute.setText(mcontext.getString(res.getResourceId(mcontext,items.getTitle(),"string")));
                    tvValue.setText(UserInformations.isContained(mcontext,items.isContained()));

                    if(items.isContained()) {
                        tvSource.setText(mcontext.getString(res.getResourceId(mcontext, items.getTitle() + "_source", "string")));
                        tvReason.setText(items.getExplanation(mcontext));
                    }else{
                        tvSource.setVisibility(View.INVISIBLE);
                        tvReason.setVisibility(View.INVISIBLE);
                    }

                    lldynamic_items.addView(dynamiLayout);
                }
                GsonHelper<User>  userGsonHelper=new GsonHelper();
                GsonHelper<DetailFarm>  FarmGsonHelper=new GsonHelper();

                User user=userGsonHelper.fromJson(SessionManager.getFromPrefs(mcontext,SessionManager.PREFS_USER_INFORMATION,""),User.class);
                DetailFarm farm=FarmGsonHelper.fromJson(SessionManager.getFromPrefs(mcontext,SessionManager.PREFS_FARM_INFORMATION,""),DetailFarm.class);
                if(farm==null)
                    farm=FirebaseManager.getFarm(mcontext);

                String[] latLongs = farm.point.split(",");
                coordinatesLabel.setText(getFormattedLocationInDegree(Double.parseDouble(latLongs[0]), Double.parseDouble(latLongs[1])));
                for(Map.Entry<String,Partner> partner:farm.partners.entrySet()){
                    countPartner++;
                    LinearLayout partnerLayout=(LinearLayout)inflater.inflate(R.layout.report_partner,null);
                    TextView name=(TextView) partnerLayout.findViewById(R.id.namepartner);
                    TextView cnpjcpf=(TextView) partnerLayout.findViewById(R.id.cpfcnpjpartner);
                    name.setText(partner.getValue().name);
                    cnpjcpf.setText(partner.getValue().cpf_cnpj);
                    partners.addView(partnerLayout);

                }
//                    partnerLabel.setText(farm.partner..getPartner_name());
                cpfcnpjLabel.setText(farm.cpf_cnpj);
                farmnameLabel.setText(farm.nameFarm);
                if(!TextUtils.isEmpty(farm.address) && farm.address.trim().length()!=0){
                    llfarmAddress.setVisibility(View.VISIBLE);
                    farmAddressLabel.setText(farm.address);
                }else{
                    llfarmAddress.setVisibility(View.GONE);
                }
                emailLabel.setText(user.email);
                nameLabel.setText(user.name);
                idLabel.setText(""+ SharedPreferenceManager.getId(mcontext));
                dateGeneration.setText(DateHelper.getDate(mcontext,R.string.report_generate));

            }
        };
    }

    public static String getFormattedLocationInDegree(double latitude, double longitude) {
        try {
            int latSeconds = (int) Math.round(latitude * 3600);
            int latDegrees = latSeconds / 3600;
            latSeconds = Math.abs(latSeconds % 3600);
            int latMinutes = latSeconds / 60;
            latSeconds %= 60;

            int longSeconds = (int) Math.round(longitude * 3600);
            int longDegrees = longSeconds / 3600;
            longSeconds = Math.abs(longSeconds % 3600);
            int longMinutes = longSeconds / 60;
            longSeconds %= 60;
            String latDegree = latDegrees >= 0 ? "N" : "S";
            String lonDegrees = longDegrees >= 0 ? "E" : "W";

            return  Math.abs(latDegrees) + "°" + latMinutes + "'" + latSeconds
                    + "\"" + latDegree +" "+ Math.abs(longDegrees) + "°" + longMinutes
                    + "'" + longSeconds + "\"" + lonDegrees;
        } catch (Exception e) {
            return ""+ String.format("%8.5f", latitude) + "  "
                    + String.format("%8.5f", longitude) ;
        }
    }


}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.GsonHelper;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.LatLng;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Partner;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ConfirmYourDataActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nameofUser)
    TextView nameofUser;
    @BindView(R.id.emailofUser)
    TextView emailofUser;
    @BindView(R.id.nameOfProperty)
    TextView nameOfProperty;
    @BindView(R.id.txAddressFarm)
    TextView txAddressFarm;
    @BindView(R.id.txcnpjCpf)
    TextView txcnpjCpf;
    @BindView(R.id.linepartner)
    View linepartner;
    @BindView(R.id.llpartners)
    LinearLayout llpartners;
    @BindView(R.id.saveUser)
    AppCompatCheckBox saveUser;
    @BindView(R.id.btnContinuar)
    Button btnContinuar;
    @BindView(R.id.content_confirm_your_data)
    LinearLayout contentConfirmYourData;
    @BindView(R.id.llAddress)
    LinearLayout llAddress;
    private String idUser;
    double lat = 0, lng = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_your_data);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        User user = FirebaseManager.getUser(this);
        idUser = SharedPreferenceManager.getId(this);
        showData(user);
        saveUser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                btnContinuar.setEnabled(isChecked);
            }
        });
        createToolbarBackArrowFinish();
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
    }

    private void showData(User user) {
        nameofUser.setText(user.name);
        emailofUser.setText(user.email);
        DetailFarm farm = FirebaseManager.getFarm(this);
        nameOfProperty.setText(farm.nameFarm);
        txAddressFarm.setText(farm.address);
        if(TextUtils.isEmpty(farm.address)){
            llAddress.setVisibility(View.GONE);
        }else{
            llAddress.setVisibility(View.VISIBLE);
        }
        txcnpjCpf.setText(farm.cpf_cnpj);
        shoPartners(farm);
    }

    private void shoPartners(DetailFarm farm) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int count = 0;
        for (Map.Entry<String, Partner> partner : farm.partners.entrySet()) {
            count++;
            LinearLayout partnerLayout = (LinearLayout) inflater.inflate(R.layout.layout_partner, null);
            TextView txcnpjCpfPartner = (TextView) partnerLayout.findViewById(R.id.txcnpjCpfPartner);
            TextView nameOfpatner = (TextView) partnerLayout.findViewById(R.id.nameOfpatner);
            TextView partnercountLabel = (TextView) partnerLayout.findViewById(R.id.partnercountLabel);
            nameOfpatner.setText(partner.getValue().name);
            txcnpjCpfPartner.setText(partner.getValue().cpf_cnpj);
            partnercountLabel.setText(partnercountLabel.getText() + " " + count);
            llpartners.addView(partnerLayout);

        }
        if (count == 0) {
            llpartners.setVisibility(View.GONE);
            linepartner.setVisibility(View.GONE);
        } else {
            llpartners.setVisibility(View.VISIBLE);linepartner.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btnContinuar)
    public void onClick() {

        lat = Double.parseDouble(SessionManager.getFromPrefs(this, SessionManager.PREF_LAT, ""));
        lng = Double.parseDouble(SessionManager.getFromPrefs(this, SessionManager.PREF_LNG, ""));
        LatLng latLng = new LatLng(lat, lng);
        GsonHelper<LatLng> gsonHelper = new GsonHelper<>();
        SessionManager.saveToPrefs(this, SessionManager.PREFS_FARM_LAT_LNG, gsonHelper.toJson(latLng));
        FirebaseManager.saveFarmLocation(latLng, idUser, new FirebaseListener() {
            @Override
            public void successAction() {
                SharedPreferenceManager.saveState(ConfirmYourDataActivity.this, StatesOfFlowApplication.CONFIRM);
                FirebaseManager.sessionUserSave(ConfirmYourDataActivity.this, SharedPreferenceManager.getId(ConfirmYourDataActivity.this), StatesOfFlowApplication.CONFIRM.getName());
                mobileAnalytics.AddEvent(ConfirmYourDataActivity.this,ConfirmYourDataActivity.this,"confirmdata.sucess");
                Intent intent = new Intent(ConfirmYourDataActivity.this, ProgressWaitDataActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

            @Override
            public void errorAction() {
                mobileAnalytics.AddEvent(ConfirmYourDataActivity.this,ConfirmYourDataActivity.this,"confirmdata.fail");
                Toast.makeText(ConfirmYourDataActivity.this, getString(R.string.erro_generic), Toast.LENGTH_LONG).show();
            }
        }, ConfirmYourDataActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            editData();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void editData() {
        SharedPreferenceManager.saveBoolean(ConfirmYourDataActivity.this, "from_choice", true);
        Intent intent = new Intent(ConfirmYourDataActivity.this, br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.DetailFarm.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}

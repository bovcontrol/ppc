package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.crash.FirebaseCrash;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.Resources;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;

public class LauchActivity extends AppCompatActivity {
    private Tracker mTracker;
    Resources res;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_inicial);
        AppEventsLogger.activateApp(this);
        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();
        Intent i=new Intent(this,DashboardActivity.class);
        startActivity(i);
    }
    @Override
    protected void onResume() {
        super.onResume();
        res= new Resources(this);
        mTracker.setScreenName(res.getLabelInResource("ga_"+getClass().getSimpleName()));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}

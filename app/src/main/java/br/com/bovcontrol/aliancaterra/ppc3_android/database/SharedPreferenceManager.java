package br.com.bovcontrol.aliancaterra.ppc3_android.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;

/**
 * Created by ricardoogliari on 10/3/16.
 */

public class SharedPreferenceManager {

    public static void saveState(Context ctx, StatesOfFlowApplication state){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor spEdit = sp.edit();
        spEdit.putString("state", state.getName());
        spEdit.commit();
    }

    public static void saveString(Context ctx, String id){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor spEdit = sp.edit();
        spEdit.putString("id", id);
        spEdit.commit();
    }

    public static void saveBoolean(Context ctx, String key, boolean value){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor spEdit = sp.edit();
        spEdit.putBoolean(key, value);
        spEdit.commit();
    }

    public static StatesOfFlowApplication getState(Context ctx){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);

        if (sp.contains("state")) {
            try {
                return StatesOfFlowApplication.valueOf(sp.getString("state", null));
            } catch (IllegalArgumentException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    public static String getId(Context ctx){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        return sp.getString("id", null);
    }

    public static boolean getBoolean(Context ctx, String key){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        return sp.getBoolean(key, false);
    }
}

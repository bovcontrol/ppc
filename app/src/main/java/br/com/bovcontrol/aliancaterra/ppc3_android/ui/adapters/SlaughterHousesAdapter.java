package br.com.bovcontrol.aliancaterra.ppc3_android.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.HashMapUtil;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.SlaugtherHouses;


/**
 * Created by fernanda on 24/10/16.
 */

public class SlaughterHousesAdapter extends RecyclerView.Adapter<SlaughterHousesAdapter.ViewHolder> {

        List<SlaugtherHouses> slaugtherHouses= new ArrayList<>();
        HashMap<String,SlaugtherHouses> hashslaugtherHouses= new HashMap<>();
        Context context;

        public SlaughterHousesAdapter(HashMap<String,SlaugtherHouses> slaugtherHouses, Context context,Location farmlocation) {

            for(Map.Entry<String,SlaugtherHouses> entry:slaugtherHouses.entrySet()){
                SlaugtherHouses SlaugtherHouses= entry.getValue();
                SlaugtherHouses.setDistancebetweenTwoPointsinKm(farmlocation);
                this.slaugtherHouses.add(SlaugtherHouses);
            }
            Collections.sort(this.slaugtherHouses, new Comparator<SlaugtherHouses>() {

                public int compare(SlaugtherHouses p1, SlaugtherHouses p2) {
                    return Float.compare(p1.getDistance(),p2.getDistance());
                }
            });

            this.hashslaugtherHouses = slaugtherHouses;
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_row_slaugtherhouses, parent, false);

            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            SlaugtherHouses model = slaugtherHouses.get(position);
            holder.txtName.setText(model.getName());
            DecimalFormat df = new DecimalFormat("0.###");
            String saida = df.format(model.getDistance());
            holder.txtDistance.setText(""+saida.replace(",",".")+" km");


        }

        @Override
        public int getItemCount() {
            return slaugtherHouses.size();
        }

    public String getItemHash(int position) {
        SlaugtherHouses model = slaugtherHouses.get(position);
        String hash= HashMapUtil.getKeyFromValue(hashslaugtherHouses,model).toString();
        return hash;
    }

    public String getItemName(int position) {
        SlaugtherHouses model = slaugtherHouses.get(position);
        return model.getName();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView txtName;
            public TextView txtDistance;



            public ViewHolder(View itemView) {
                super(itemView);
                txtName = (TextView) itemView.findViewById(R.id.txtName);
                txtDistance=(TextView) itemView.findViewById(R.id.txtDistance);

            }
        }
}

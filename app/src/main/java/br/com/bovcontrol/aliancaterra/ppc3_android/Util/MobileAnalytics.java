package br.com.bovcontrol.aliancaterra.ppc3_android.Util;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;

/**
 * Created by fernanda on 30/07/17.
 */

public class MobileAnalytics {


    public void AddEvent(Context context, Activity activity, String action ){
        Mixpanel.addMixPanel(activity, action);
    }

    public void AddEvent(Context context,Activity activity,String action,HashMap<String, String> hash ){
        Mixpanel.addMixPanel(activity, context,action,hash);
    }

    public String getDistinctId() {
        return MainApplication.mixpanel.getDistinctId();
    }


    public  void initialize(Context context){
        Mixpanel.initializeMixPanel(context);
    }



    public void signUp(Context context, String user_id){
        Mixpanel.signupMixPanel(context, SessionManager.getFromPrefs(context, SessionManager.PREFS_USER_ID_MIXPANEL, ""));
    }
    public void flush(){
        MainApplication.mixpanel.flush();
    }


    public void logout(Context context) {
        Mixpanel.LogOut(context);
    }
}
package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters;

/**
 * Created by fernanda on 23/09/16.
 */

public interface BasePresenter {
    void resume();

    void pause();

    void stop();

    void destroy();

    void onError(String message);
}

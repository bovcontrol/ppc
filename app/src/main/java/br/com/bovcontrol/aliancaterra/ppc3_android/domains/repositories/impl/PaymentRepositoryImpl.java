package br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.PaymentRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.PaymentPresenter;

/**
 * Created by fernanda on 26/10/16.
 */

public class PaymentRepositoryImpl implements PaymentRepository {
    Context context;
    PaymentPresenter presenter;
    public PaymentRepositoryImpl(Context context, PaymentPresenter presenter){
        this.context=context;
        this.presenter=presenter;
    }

    @Override
    public void saveRequestPayment(String required,FirebaseListener listener, String idUser) {
        FirebaseManager.saveRequestPayment(required,context, listener,idUser);
    }



    @Override
    public void getToken(String userId,String idFarm) {
        final Query query = MainApplication.myRef.child("reports").child(userId).child(idFarm);
        query.addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                            Log.w("ENTROU", "ADDED");
                                            if(dataSnapshot.getKey().equals("payment_session") && dataSnapshot.getValue()!=null && (!TextUtils.isEmpty(dataSnapshot.getValue().toString()))) {
                                                presenter.successGetToken(dataSnapshot.getValue().toString());
                                                query.removeEventListener(this);
                                            }
                                        }

                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                            if(dataSnapshot.getKey().equals("payment_session") && dataSnapshot.getValue()!=null && (!TextUtils.isEmpty(dataSnapshot.getValue().toString()))) {
                                                presenter.successGetToken(dataSnapshot.getValue().toString());
                                                query.removeEventListener(this);
                                            }


                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                                            Log.w("ENTROU", "REMOVED");
                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                                            Log.w("ENTROU", "MOVED");
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            presenter.errorGetToken("Unexpected Error");
                                            query.removeEventListener(this);
                                        }
                                    }
        );
    }
}

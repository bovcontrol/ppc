package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bovcontrol.signon.widget.Auth.AuthHelper;
import com.bovcontrol.signon.widget.Encrypt;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Collection;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.MobileAnalytics;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.PPCAuthHelper;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;

/**
 * Created by ricardoogliari on 9/28/16.
 */

public class AuthActivity extends BaseActivity {
    Encrypt  encryption = Encrypt.getDefault(Encrypt.createKey(), Encrypt.createSalt(), new byte[16]);
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    MobileAnalytics mobileAnalytics= new MobileAnalytics();
    private final String TAG = "alianca_terra";



    private LoginManager loginManager;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        callbackManager = CallbackManager.Factory.create();

        loginManager = LoginManager.getInstance();
        loginManager.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Profile profile = Profile.getCurrentProfile();
//                        Log.e(TAG, "sucesso no logo do face profile.name: " + profile.getName());
                       String name="";
                        if(profile!=null && (!TextUtils.isEmpty(profile.getName()))){
                            name=profile.getName();
                        }
                        firebaseAuthWithFace(loginResult.getAccessToken(), name);
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e(TAG, "errooooooo no logo do face " + exception.getLocalizedMessage());
                    }
                });
    }

    protected void createUserWithEmailAndPassword (final String email, final String password, final String name){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            User user = new User();
                            user.email = email;
                            user.name = name;
                            FirebaseManager.saveuser(user, task.getResult().getUser().getUid(),AuthActivity.this);
                            SessionManager.saveToPrefs(AuthActivity.this,SessionManager.PREFS_USER_ID_MIXPANEL,""+task.getResult().getUser().getUid());
                            savePPCAccount(email,password,name);
                            initMixpanel(task.getResult().getUser().getUid());
//                            mobileAnalytics.signUp(AuthActivity.this,task.getResult().getUser().getUid());
                            mobileAnalytics.AddEvent(AuthActivity.this,AuthActivity.this,"createaccount.email.success");
                            FirebaseManager.saveuser(user, task.getResult().getUser().getUid(),AuthActivity.this);

                        }

                        if (!task.isSuccessful()) {
                            Log.e("TERRA", "exception: " + task.getException().getLocalizedMessage());
                            Toast.makeText(AuthActivity.this, getString(R.string.create_user_error),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void initMixpanel(String uuid) {
        mobileAnalytics.initialize(this);
        SessionManager.saveToPrefs(this,SessionManager.PREFS_USER_ID_MIXPANEL,uuid);
        mobileAnalytics.signUp(AuthActivity.this,uuid);
    }

    protected void firebaseAuthWithFace(final AccessToken token, final String name) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull final Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            GraphRequest request = GraphRequest.newMeRequest(
                                    token,
                                    new GraphRequest.GraphJSONObjectCallback() {
                                        @Override
                                        public void onCompleted(JSONObject object, GraphResponse response) {
                                            String email = object.optString("email");

                                            User user = new User();
                                            user.email = email;
                                            user.name = name;
                                            saveUserinSession(user.email, "", user.name);
                                            SessionManager.saveToPrefs(AuthActivity.this,SessionManager.PREFS_USER_ID_MIXPANEL,""+task.getResult().getUser().getUid());
                                            FirebaseManager.saveuser(user, task.getResult().getUser().getUid(),AuthActivity.this);
                                            initMixpanel(task.getResult().getUser().getUid());
                                            mobileAnalytics.AddEvent(AuthActivity.this,AuthActivity.this,"createaccount.face.success");

                                        }
                                    });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "email");
                            request.setParameters(parameters);
                            request.executeAsync();
                        }

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(AuthActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    protected void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    /*@Override
                    public void onComplete(@NonNull Tasks<AuthResult> task) {

                    }*/

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            User user = new User();
                            user.email = acct.getEmail();
                            user.name = acct.getDisplayName();
                            saveUserinSession(user.email, "", user.name);
                            FirebaseManager.saveuser(user, task.getResult().getUser().getUid(), AuthActivity.this);
                            SessionManager.saveToPrefs(AuthActivity.this,SessionManager.PREFS_USER_ID_MIXPANEL,""+task.getResult().getUser().getUid());
                            initMixpanel(task.getResult().getUser().getUid());
                            mobileAnalytics.AddEvent(AuthActivity.this,AuthActivity.this,"createaccount.google.success");

                        }
                    }
                });
    }

    protected void firebaseSignWithEmail(final String email, final String password, final View view, final AppCompatCheckBox checkbox, final boolean isbov){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            if(checkbox!=null && checkbox.isChecked()) {
                                SessionManager.saveToPrefs(AuthActivity.this,SessionManager.PREF_SAVE_LOGIN,"true");
                                saveUserinSession(email, password, email);
                            }
                            FirebaseManager.goDbScreen(task.getResult().getUser().getUid(), AuthActivity.this);
                            savePPCAccount(email,password,email);
                            SessionManager.saveToPrefs(AuthActivity.this,SessionManager.PREFS_USER_ID_MIXPANEL,""+task.getResult().getUser().getUid());
                            initMixpanel(task.getResult().getUser().getUid());
                            if(isbov) {
                                mobileAnalytics.AddEvent(AuthActivity.this, AuthActivity.this, "buttonbovcontrol.success");
                            }else{
                                mobileAnalytics.AddEvent(AuthActivity.this, AuthActivity.this, "signin.email.success");
                            }
                            //showDetailActivity(task.getResult().getUser().getUid());
                        } else if (task.getException() != null){
                            Snackbar.make(view, task.getException().getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void savePPCAccount(String email, String password,String name) {
        saveUserinSession(email,password,email);
        PPCAuthHelper helper= new PPCAuthHelper();
        try {
            Log.e("CATCH AUTH","CATCH AUTH VAIII");
            helper.addAccount(email, encryption.encrypt(password),this);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("CATCH AUTH","CATCH AUTH"+e.getMessage());
        }
    }

    public void saveUserinSession(String email, String password,String name){

        SessionManager.saveToPrefs(this,SessionManager.PREF_SAVE_EMAIL,email);
        SessionManager.saveToPrefs(this,SessionManager.PREF_SAVE_PASSWORD,password);
        SessionManager.saveToPrefs(this,SessionManager.PREF_SAVE_NAME,name);
    }

    public void showDetailActivity(String id){
        SharedPreferenceManager.saveString(this, id);

        Intent intent = new Intent(this, DetailFarm.class);
        intent.putExtra("id", id);
        startActivity(intent);
        finish();
    }

    public void loginByFace(){
        Collection<String> permissions = Arrays.asList("public_profile", "user_friends", "email");
        loginManager.logInWithReadPermissions(this, permissions); // Null Pointer Exception here
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == 1) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void loginByGoogle(){
        GoogleSignInOptions gso = new
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, 1);
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.auth.FirebaseAuth;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.Mixpanel;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.MobileAnalytics;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.Resources;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;

/**
 * Created by fernanda on 23/09/16.
 */

public class BaseActivity  extends AppCompatActivity{
    protected MobileAnalytics mobileAnalytics= new MobileAnalytics();
    private Tracker mTracker;
    Resources res;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();

    }

    @Override
    protected void onResume() {
        super.onResume();
        res= new Resources(this);
        String label=res.getLabelInResource("ga_"+getClass().getSimpleName());
        mTracker.setScreenName(label);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mobileAnalytics.AddEvent(this,this,getClass()+".open");
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    Toolbar toolbar;
    protected Toolbar createToolbarBackArrow() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setSupportActionBar(toolbar);

        return toolbar;
    }
    protected Toolbar createToolbarBackArrowFinish() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        return toolbar;
    }

    protected Toolbar createToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        return toolbar;
    }

    protected void logoff(){
        SharedPreferenceManager.saveState(this, StatesOfFlowApplication.LOGOFF);
        mobileAnalytics.logout(this);
//        FirebaseManager.sessionUserSave(this, SharedPreferenceManager.getId(this), StatesOfFlowApplication.LOGOFF.getName());
        SessionManager.DeleteAllPrefs(this);
        FirebaseAuth.getInstance().signOut();

        Intent intent = new Intent(this, PresentationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.only_logoff, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.miLogoff) {
            logoff();
            return true;
        } else if (item.getItemId() == R.id.miMoreApps){
            openBovcontrolOnboarding();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void openBovcontrolOnboarding() {
        Intent intent= new Intent(BaseActivity.this,OnboardingActivityBovcontrol.class);
        startActivity(intent);
    }

}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bovcontrol.signon.widget.BovLoginButton;
import com.bovcontrol.signon.widget.Click.ClickCallback;
import com.bovcontrol.signon.widget.models.BovControlResponse;

import java.util.Random;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignActivity extends AuthActivity {

    @BindView(R.id.txtSignInEmail)
    TextInputEditText edtEmail;

    @BindView(R.id.txtSignInPass)
    TextInputEditText edtPass;

    @BindView(R.id.inputLayoutSignInPass)
    TextInputLayout inputLayoutPass;

    @BindView(R.id.inputLayoutSignInEmail)
    TextInputLayout inputLayoutEmail;
    @BindView(R.id.tvForgotPassword)
    TextView tvForgotPassword;
    @BindView(R.id.saveUser)
    AppCompatCheckBox saveUser;
    @BindView(R.id.BovLoginButton)
    com.bovcontrol.signon.widget.BovLoginButton BovLoginButton;
    @BindView(R.id.content_enter_with_email_pass)
    LinearLayout contentEnterWithEmailPass;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BovLoginButton.onPause();
    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);

        ButterKnife.bind(this);

        createToolbarBackArrow();
        BovLoginButton.OnResume();
        BovLoginButton.setCallback(new ClickCallback() {
            @Override
            public void getAccountInformation(BovControlResponse bovControlResponse) {
                String farm_id=bovControlResponse.getFarm_id();
                if(!TextUtils.isEmpty(farm_id)) {
                    firebaseSignWithEmail(bovControlResponse.getEmail(), bovControlResponse.getFarm_id(),contentEnterWithEmailPass, null, true);
                }else{
                    Toast.makeText(SignActivity.this, getString(R.string.invalid_account), Toast.LENGTH_SHORT).show();
                }

            }
        });
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutEmail.setErrorEnabled(false);
                inputLayoutEmail.setError(null);
            }
        });

        edtPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutPass.setErrorEnabled(false);
                inputLayoutPass.setError(null);
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignActivity.this, RecoverPasswordActivity.class);
                startActivity(i);
            }
        });
        if ("true".equals(SessionManager.getFromPrefs(this, SessionManager.PREF_SAVE_LOGIN, ""))) {
            String email = SessionManager.getFromPrefs(this, SessionManager.PREF_SAVE_EMAIL, "");
            String password = SessionManager.getFromPrefs(this, SessionManager.PREF_SAVE_PASSWORD, "");
            edtEmail.setText(email);
            edtPass.setText(password);
            saveUser.setChecked(true);
        }

    }

    @OnClick(R.id.btnSignIn)
    public void signIn(View view) {
        if (validateFields()) {
            mobileAnalytics.AddEvent(this,this,"button.signin");
            firebaseSignWithEmail(edtEmail.getText().toString(), edtPass.getText().toString(), view, saveUser,false);
        }
    }

    @OnClick(R.id.btnSignFacebook)
    public void signInFace(View view) {
        mobileAnalytics.AddEvent(this,this,"button.signin.facebook");loginByFace();
    }

    @OnClick(R.id.btnSignGoogle)
    public void signInGoogle(View view) {
        mobileAnalytics.AddEvent(this,this,"button.signin.google");
        loginByGoogle();
    }

    public boolean validateFields() {
        boolean retorno = true;

        String email = edtEmail.getText().toString();
        String pass = edtPass.getText().toString();

        if (pass.trim().length() < 6) {
            inputLayoutPass.setError(getString(R.string.password_at_least));
            retorno = false;
        }

        if (email.trim().length() < 1) {
            inputLayoutEmail.setError(getString(R.string.email_must_filled));
            retorno = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            inputLayoutEmail.setError(getString(R.string.incorrect_email));
            retorno = false;
        }

        return retorno;
    }

}

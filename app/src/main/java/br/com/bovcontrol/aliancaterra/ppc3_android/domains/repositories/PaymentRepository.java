package br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories;

import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;

/**
 * Created by fernanda on 26/10/16.
 */

public interface PaymentRepository {
    public void saveRequestPayment(String required,FirebaseListener listener, String idUser);
    public void getToken(String userId,String idFarm);
}

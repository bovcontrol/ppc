package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters;

import android.content.Context;

import java.util.HashMap;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.SlaugtherHouses;

/**
 * Created by fernanda on 25/10/16.
 */

public interface SlaugtherhousesPresenter {
    public void getSlaughters();

    void setList(HashMap<String, SlaugtherHouses> value);

    void save(String uuidSlaugther, String idUser, Context context);
    void successTogenerateCoupon();
    void failTogenerateCoupon();
}

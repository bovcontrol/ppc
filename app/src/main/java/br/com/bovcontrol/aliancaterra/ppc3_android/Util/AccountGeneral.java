package br.com.bovcontrol.aliancaterra.ppc3_android.Util;

/**
 * Created by fernanda on 19/07/17.
 */

public class AccountGeneral {
    /**
     * Auth token types
     */
    public static final String AUTHTOKEN_TYPE_READ_ONLY = "Read only";
    public static final String AUTHTOKEN_TYPE_READ_ONLY_LABEL = "Read only access to an bovcontrol account";

    public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";
    public static final String AUTHTOKEN_TYPE_FULL_ACCESS_LABEL = "Full access to an bovcontrol account";
}

package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views;

import android.app.Application;

import java.io.File;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;

/**
 * Created by fernanda on 27/09/16.
 */

public interface CertificateView extends  BaseView {
    public Application getApplication ();
    void showData(Report report);
    void sucessPdfGenerator(File path);
    void changeBackgound(boolean isContained);

    void onErrorPdfGenerated();
}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InitialScreen extends BaseActivity {

    @BindView(R.id.tvterms)
    TextView tvterms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatesOfFlowApplication.showCurrentScreen(SharedPreferenceManager.getState(this), this);
        setContentView(R.layout.activity_tela_inicial);
        ButterKnife.bind(this);

//        showHashKey(this);

        tvterms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(InitialScreen.this,WebviewTermsAndConditions.class);
                startActivity(intent);
            }
        });
    }


    @OnClick(R.id.btnEntrar)
    public void entrar(View view) {
        mobileAnalytics.AddEvent(this,this,"button.enter");
        Intent intent = new Intent(this, PresentationActivity.class);
        startActivity(intent);
        finish();
    }

}

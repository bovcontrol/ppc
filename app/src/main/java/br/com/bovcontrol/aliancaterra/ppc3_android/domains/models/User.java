package br.com.bovcontrol.aliancaterra.ppc3_android.domains.models;

import android.content.Context;

import com.google.firebase.database.Exclude;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;

/**
 * Created by ricardoogliari on 10/1/16.
 */

public class User {

    public String name;
    public String email;
    public String language;
    public HashMap<String,DetailFarm> farms= new HashMap<>();
    @Exclude
    public HashMap<String,String> codeAndNameFarm= new HashMap<>();

    public String sessionName;
    public String sessionFarm;
    public String coupon;

    public void setSession(String sessionName){
        this.sessionName = sessionName;
    }

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
        this.language= Locale.getDefault().getLanguage();
//        this.farmList=farmList;
    }
    public void setFarm(DetailFarm list,Context context){
        String  uniqueID = UUID.randomUUID().toString();
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        uniqueID+=""+timeStamp;
        SessionManager.saveToPrefs(context,SessionManager.PREFS_FARM_UNIQUE_ID,uniqueID);
        sessionFarm=uniqueID;
        farms.put(uniqueID,list);
        this.language= Locale.getDefault().getLanguage();
    }
    @Exclude
    public List<String> getFarmNames() {
        List<String> list=new ArrayList<>();
        for(Map.Entry<String, DetailFarm> entry : farms.entrySet()) {
            String key = entry.getKey();
            DetailFarm value = entry.getValue();
            sessionFarm=key;
            codeAndNameFarm.put(value.nameFarm,key);
            list.add(value.nameFarm);
        }
        return list;
    }
    @Exclude
    public int getPositionByFarmId(String farmId,List<String> nameFarm){
        for(Map.Entry<String, DetailFarm> entry : farms.entrySet()) {
            if(entry.getKey().equals(farmId)){
                return nameFarm.indexOf(entry.getValue().nameFarm);
            }
        }
        return 0;
    }
    @Exclude
    public void UpdateFarm(String farmId, DetailFarm  detailfarm) {
        farms.put(farmId,detailfarm);
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.CupomPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.CupomPresenterImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.CupomView;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StatusBarUtil;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.VectorDrawableUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CupomActivity extends BaseActivity implements CupomView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnContinuar)
    Button btnContinuar;
    @BindView(R.id.edcupom)
    EditText edcupom;
    @BindView(R.id.initiallayout)
    LinearLayout initiallayout;
    @BindView(R.id.imgResult)
    ImageView imgResult;
    @BindView(R.id.validateLayout)
    LinearLayout validateLayout;
    @BindView(R.id.llcontainedcenter)
    LinearLayout llcontainedcenter;
    VectorDrawableUtils vectorDrawableUtils;
    CupomPresenter presenter;
    @BindView(R.id.tvvalidate)
    TextView tvvalidate;
    @BindView(R.id.content_enter_with_email_pass)
    LinearLayout contentEnterWithEmailPass;
    @BindView(R.id.scrollCupom)
    ScrollView scrollCupom;
    ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cupom);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        createToolbarBackArrow();
        presenter = new CupomPresenterImpl(this);
        vectorDrawableUtils = new VectorDrawableUtils();
        setuValidationInEdCupom();
        Window window = getWindow();
        StatusBarUtil.changeStatusBar(window, getResources().getColor(R.color.brown));

    }

    private void setuValidationInEdCupom() {
        edcupom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                long count = edcupom.getText().toString().length();
                if (count >= 6) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edcupom.getWindowToken(), 0);
                    String cupom=edcupom.getText().toString();
                    cupom = cupom.replaceAll("\\r|\\n", "");
                    presenter.validateCupom(cupom,CupomActivity.this, SharedPreferenceManager.getId(CupomActivity.this), SessionManager.getFromPrefs(CupomActivity.this,SessionManager.PREFS_FARM_UNIQUE_ID,""));

                }
            }
        });
    }

    @Override
    public void showCupomValide() {
        btnContinuar.setEnabled(true);
        imgResult.setImageDrawable(vectorDrawableUtils.generateVactorDrawable(this, R.drawable.cupom_validate));
        edcupom.setEnabled(false);
        initiallayout.setVisibility(View.GONE);
        validateLayout.setVisibility(View.VISIBLE);
        tvvalidate.setText(getString(R.string.cupom_valid));
        tvvalidate.setTextColor(getResources().getColor(R.color.white));
        changeBackground(true);

    }

    @Override
    public void showCupomInvalide() {
        edcupom.clearFocus();
        btnContinuar.setEnabled(false);
        imgResult.setImageDrawable(vectorDrawableUtils.generateVactorDrawable(this, R.drawable.cupom_invalidate));
        edcupom.setEnabled(true);
        initiallayout.setVisibility(View.GONE);
        validateLayout.setVisibility(View.VISIBLE);
        tvvalidate.setText(getString(R.string.cupom_invalid));
        tvvalidate.setTextColor(getResources().getColor(R.color.red));
        changeBackground(false);

    }

    public void changeBackground(boolean valid) {
        int color = 0;
        int color_dark = 0;
        if (!valid) {
            color = getResources().getColor(R.color.colorAccent);
            color_dark = getResources().getColor(R.color.brown);
        } else {
            color = getResources().getColor(R.color.colorPrimary);
            color_dark = getResources().getColor(R.color.colorPrimaryDark);
        }
        scrollCupom.setBackgroundColor(color);
        toolbar.setBackgroundColor(color);
        Window window = getWindow();
        StatusBarUtil.changeStatusBar(window, color_dark);
    }

    @Override
    public void showLoading() {
        mDialog = new ProgressDialog(this);
        mDialog.setTitle(getString(R.string.please_wait));
        mDialog.setIndeterminate(false);
        mDialog.setCancelable(false);
        mDialog.show();
    }

    @Override
    public void hideLoading() {
        if ((mDialog != null) && mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    public void showErrorMessage(String message) {

    }

    @Override
    public void showWihoutNetwork() {

    }

    @OnClick(R.id.btnContinuar)
    public void onClick() {
        mobileAnalytics.AddEvent(this,this,"cupomActivity.nextbutton");
        Intent i= new Intent(this,PostOrPayActivity.class);
        SharedPreferenceManager.saveState(CupomActivity.this, StatesOfFlowApplication.CUPOM);
        startActivity(i);
        finish();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}

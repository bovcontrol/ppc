package br.com.bovcontrol.aliancaterra.ppc3_android.ui.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;

/**
 * Created by fernanda on 28/09/16.
 */

public class VectorDrawableUtils {

    public Drawable generateVactorDrawable(Context context, int resource){
        Drawable bg;
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                bg = VectorDrawableCompat.create(context.getResources(), resource, null);
            } else {
                bg = ContextCompat.getDrawable(context, resource);
            }
        }catch(Exception e)
        {
            bg=context.getResources().getDrawable(R.drawable.ic_contains_warning);
        }
        return bg;
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

/**
 * Created by fernanda on 30/09/16.
 */

public class SessionManager {
    public static final String  PREFS_FARM_LAT_LNG="PREFS_FARM_LAT_LNG";
    public static final String  PREFS_USER_INFORMATION="PREFS_USER_INFORMATION";
    public static final String REPORT_INFORMATION = "REPORT_INFORMATION";
    public static final String PREFS_FARM_INFORMATION = "FARM_INFORMATION";
    public static final String PREFS_FARM_UNIQUE_ID="PREFS_FARM_UNIQUE_ID";
    public static final String PREFS_ACTIVITY_SHOW_REPORT="PREFS_ACTIVITY_SHOW_REPORT";
    public static final String PREF_USER_FIRST_TIME="PREF_USER_FIRST_TIME";
    public static final String PREF_SAVE_LOGIN="PREF_SAVE_LOGIN";
    public static final String PREF_SAVE_EMAIL="PREF_SAVE_EMAIL";
    public static final String PREF_SAVE_PASSWORD="PREF_SAVE_PASSWORD";
    public static final String PREF_LAT="PREF_LAT";
    public static final String PREF_LNG="PREF_LNG";
    public static final String PREFS_USER_ID_MIXPANEL="PREFS_USER_ID_MIXPANEL";
    public static final String PREF_SAVE_NAME="PREF_SAVE_NAME";
    public static final String PREFS_BovControlONBOARDING="PREFS_BovControlONBOARDING";

    /**
     * Called to save supplied value in shared preferences against given key.
     * @param context Context of caller activity
     * @param key Key of value to save against
     * @param value Value to save
     */
    public static void saveToPrefs(Context context, String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key,value);
        editor.commit();

    }

    /**
     * Called to retrieve required value from shared preferences, identified by given key.
     * Default value will be returned of no value found or error occurred.
     * @param context Context of caller activity
     * @param key Key to find value against
     * @param defaultValue Value to return if no data found against given key
     * @return Return the value found against given key, default if not found or any error occurs
     */
    public static String getFromPrefs(Context context, String key, String defaultValue) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        try {

            return sharedPrefs.getString(key, defaultValue);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }
    public static void DeleteAllPrefs(Context context) {

        String email=SessionManager.getFromPrefs(context,SessionManager.PREF_SAVE_EMAIL,"");
        String password=SessionManager.getFromPrefs(context,SessionManager.PREF_SAVE_PASSWORD,"");
        String login=SessionManager.getFromPrefs(context,SessionManager.PREF_SAVE_LOGIN,"");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
        saveToPrefs(context,PREF_USER_FIRST_TIME,"false");
        saveToPrefs(context,PREF_SAVE_LOGIN,login);
        saveToPrefs(context,PREF_SAVE_EMAIL,email);
        saveToPrefs(context,PREF_SAVE_PASSWORD,password);
    }

    public static void DeletePref(Context context, String key){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = sharedPrefs.edit();
        if(sharedPrefs.contains(key)){
            editor.remove(key);
            editor.commit();
        }
    }


}

package br.com.bovcontrol.aliancaterra.ppc3_android.Util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.SplashScreenActivity;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.WebViewNotification;

/**
 * Created by fernanda on 01/08/17.
 */

public class DeepLinkHelper {
    public static boolean verifyDeepLinking(Uri inputUri) {
        return inputUri != null;
    }

    public static void openActivity(Uri data, Activity act) {
        boolean isNotification = false;

        String url = data.toString().replace("https://", "");
        String path = "";
        if (!url.contains("http")) {
            path = url.substring(url.indexOf("/") + 1);
        } else {
            path = url;
        }


        getActivity(path, act, isNotification);
//        act.finish();


    }

    public static Intent getActivity(String pathurl, Activity activity, boolean isNotification) {

        return new Intent(activity, SplashScreenActivity.class);


    }
}

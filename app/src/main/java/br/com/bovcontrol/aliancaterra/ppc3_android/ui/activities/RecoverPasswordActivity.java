package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.RecoverPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.RecoverPasswordView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecoverPasswordActivity extends BaseActivity implements RecoverPasswordView {

    @BindView(R.id.txtSignInEmail)
    TextInputEditText txtSignInEmail;
    @BindView(R.id.inputLayoutSignInEmail)
    TextInputLayout inputLayoutSignInEmail;
    @BindView(R.id.message_after_send_email)
    TextView messageAfterSendEmail;
    @BindView(R.id.content_enter_with_email_pass)
    LinearLayout contentEnterWithEmailPass;
    @BindView(R.id.btnContinuar)
    Button btnContinuar;
    RecoverPresenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnSend)
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_password);
        ButterKnife.bind(this);
        presenter = new RecoverPresenter(this);
        messageAfterSendEmail.setVisibility(View.GONE);
        txtSignInEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutSignInEmail.setError(null);
            }
        });
        createToolbarBackArrow();
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        btnSend.setVisibility(View.VISIBLE);
        btnContinuar.setVisibility(View.GONE);
    }

    @Override
    public void SendEmailToRecover() {
        presenter.sendRecoverPassword(txtSignInEmail.getText().toString());
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txtSignInEmail.getWindowToken(), 0);
        btnSend.setVisibility(View.GONE);
        btnContinuar.setVisibility(View.VISIBLE);
    }

    @Override
    public void sucessTorecover() {
        messageAfterSendEmail.setVisibility(View.VISIBLE);
        messageAfterSendEmail.setText(getString(R.string.message_recover_password));
    }

    @Override
    public void failToRecover() {
        messageAfterSendEmail.setVisibility(View.VISIBLE);
        messageAfterSendEmail.setText(getString(R.string.message_recover_password_error));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private boolean validForm() {
        boolean retorno = true;
        String email = txtSignInEmail.getText().toString();

        if (email.trim().length() < 1) {
            inputLayoutSignInEmail.setError(getString(R.string.email_must_filled));
            retorno = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            inputLayoutSignInEmail.setError(getString(R.string.incorrect_email));
            retorno = false;
        }
        return retorno;
    }

    @OnClick({R.id.btnSend, R.id.btnContinuar})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSend:
                if (validForm()) {
                    mobileAnalytics.AddEvent(this,this,"button.recoverpassword");
                    SendEmailToRecover();
                }
                break;
            case R.id.btnContinuar:
                mobileAnalytics.AddEvent(this,this,"button.next.recorverpassword");
                finish();
                break;
        }
    }
}

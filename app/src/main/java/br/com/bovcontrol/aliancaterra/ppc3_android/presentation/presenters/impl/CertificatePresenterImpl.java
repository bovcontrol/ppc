package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.io.File;
import java.util.List;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.ReportItems;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.ReportRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl.ReportRepositoryImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.BasePresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.CertificatePresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.CertificateView;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.DashboardActivity;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.PdfGenerator;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StringUtils;

/**
 * Created by fernanda on 26/09/16.
 */

public class CertificatePresenterImpl implements CertificatePresenter, BasePresenter {

    ReportRepository reportRepository;

    CertificateView view;
    Boolean iscreating=false;


    public CertificatePresenterImpl(){

    }

    public void obtainView(CertificateView view){
        this.view = view;
        reportRepository = new ReportRepositoryImpl(this);
    }

    @Override
    public void isContained(List<ReportItems> itensCertificate) {
        boolean isContained=false;
        Integer count=0;
        for (ReportItems item:itensCertificate) {
            if(item.isContained())
                count+=1;
        }
        if(count!=0)
            isContained=true;

        view.changeBackgound(isContained);
    }

    @Override
    public void loadData(String userId,String idFarm) {
        try {
            view.showLoading();
            this.reportRepository.getReport(userId,idFarm,view.getApplication().getApplicationContext());
//            view.showData(reportRepository);

        }catch(Exception e){
            Log.d("Error:",""+e.getMessage());

        }


    }

    @Override
    public void SuccesloadData(Report report) {
        view.showData(report);
        view.hideLoading();
    }

    @Override
    public void failloadData(String erro) {
        view.showErrorMessage(erro);
        view.hideLoading();
    }


    @Override
    public void generatePdf(final Report report) {
        view.showLoading();
        if(!iscreating) {
            iscreating=true;
            try {
                PdfGenerator pdfGenerator = new PdfGenerator(((Activity) view), report.getName().replace(" ", "") + StringUtils.getSalt());
                com.hendrix.pdfmyxml.PdfDocument doc = pdfGenerator.generatePdf(pdfGenerator.getAbstractViewRenderer(report));

                doc.setListener(new com.hendrix.pdfmyxml.PdfDocument.Callback() {
                    @Override
                    public void onComplete(File file) {
                        Log.d(com.hendrix.pdfmyxml.PdfDocument.TAG_PDF_MY_XML, file.getAbsolutePath());
                        Log.i(com.hendrix.pdfmyxml.PdfDocument.TAG_PDF_MY_XML, "Complete");
                        onSucessPdfGenerated(file);
                        iscreating=false;

                    }

                    @Override
                    public void onError(Exception e) {
                        Log.i(com.hendrix.pdfmyxml.PdfDocument.TAG_PDF_MY_XML, "Error" + e.getMessage());
                        onErrorPdfGenerated(e.getMessage());
                        iscreating=false;
                    }
                });

                if(view instanceof DashboardActivity)
                    doc.createPdf(((Activity) view).getParent());
                else
                    doc.createPdf(((Activity) view).getParent());

            } catch (Exception e) {
                onErrorPdfGenerated(e.getMessage());
                iscreating=false;
            }
        }


    }

    @Override
    public void onSucessPdfGenerated(File path) {
        view.hideLoading();
        view.sucessPdfGenerator(path);
    }

    @Override
    public void onErrorPdfGenerated(String message) {
        view.hideLoading();
        view.onErrorPdfGenerated();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {
        view.hideLoading();
        view.showErrorMessage(message);
    }

    public boolean removeAtualFarm(final Context context){
        boolean retorno=true;
        try {
            User user = FirebaseManager.getUser(context);
            user.farms.remove(SessionManager.getFromPrefs(context,SessionManager.PREFS_FARM_UNIQUE_ID,""));
            FirebaseManager.saveUserSession(user,context);
            FirebaseManager.updateUser(user, SharedPreferenceManager.getId(context), new FirebaseListener() {
                @Override
                public void successAction() {

                }

                @Override
                public void errorAction() {
                    failloadData(context.getString(R.string.erro_generic));
                }
            },context);

        }catch(Exception e){
            return false;
        }
        return retorno;

    }
}

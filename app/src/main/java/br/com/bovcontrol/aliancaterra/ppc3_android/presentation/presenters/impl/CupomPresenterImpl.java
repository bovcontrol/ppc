package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl;

import android.content.Context;

import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.CupomPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.CupomView;

/**
 * Created by fernanda on 24/10/16.
 */

public class CupomPresenterImpl implements CupomPresenter {

    CupomView view;

    public CupomPresenterImpl(CupomView view){
        this.view=view;
    }

    @Override
    public void validateCupom(final String cupom, final Context context,final String idUser, final String farmId) {
        view.showLoading();
        FirebaseManager.saveValidatePromocode(cupom, context, new FirebaseListener() {
            @Override
            public void successAction() {
               listnerChanges(cupom,context,idUser,farmId);
            }

            @Override
            public void errorAction() {
                view.showCupomInvalide();
                view.hideLoading();
            }
        },idUser);

    }

    private void listnerChanges(String cupom, Context context, String idUser, String farmId) {
        FirebaseManager.getCupomValue(cupom,idUser,farmId,new FirebaseListener(){

            @Override
            public void successAction() {
                view.showCupomValide();
                view.hideLoading();
            }

            @Override
            public void errorAction() {
                view.showCupomInvalide();
                view.hideLoading();
            }
        });
    }
}

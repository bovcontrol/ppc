package br.com.bovcontrol.aliancaterra.ppc3_android.domains.models;

/**
 * Created by fernanda on 04/10/16.
 */

public class Position {
    String position;
    public Position(LatLng latLng){
        this.position=latLng.getLng()+","+latLng.getLat();
    }
}

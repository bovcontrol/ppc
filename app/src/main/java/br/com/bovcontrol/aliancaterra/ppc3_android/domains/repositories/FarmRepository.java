package br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories;

import android.content.Context;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.UserInformations;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.LatLng;

/**
 * Created by fernanda on 27/09/16.
 */

public interface FarmRepository {
    public UserInformations getFarmInformations();

    LatLng getLatLng(Context context);

}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.PaymentPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.PaymentPresenterImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.PaymentView;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StatusBarUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PostOrPayActivity extends BaseActivity implements PaymentView {

    @BindView(R.id.btnPost)
    Button btnPost;
    @BindView(R.id.btnPay)
    Button btnPay;

    CallbackManager callbackManager;
    ShareDialog shareDialog;
    @BindView(R.id.clPayorShare)
    CoordinatorLayout clPayorShare;
    PaymentPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_or_pay);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Window window = getWindow();
        StatusBarUtil.changeStatusBar(window, getResources().getColor(R.color.gray));
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        presenter=new PaymentPresenterImpl(this,this);
    }

    @OnClick({R.id.btnPost, R.id.btnPay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPost:
                mobileAnalytics.AddEvent(this,this,"button.postfacebook");
                configureAndShare();
                break;
            case R.id.btnPay:
                mobileAnalytics.AddEvent(this,this,"button.pay");
                Intent intent = new Intent(this, PaymentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void configureAndShare() {
        shareDialog.registerCallback(callbackManager, getCallback());
        if (ShareDialog.canShow(ShareLinkContent.class)) {

            shareDialog.show(getShare());
        }
    }

    @NonNull
    private FacebookCallback<Sharer.Result> getCallback() {
        return new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {

                presenter.saveRequest_payment("false");
            }

            @Override
            public void onCancel() {
                Snackbar.make(clPayorShare,getString(R.string.cancel_share),Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Snackbar.make(clPayorShare,getString(R.string.erro_generic),Snackbar.LENGTH_LONG).show();
            }
        };
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public ShareLinkContent getShare() {
        ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(getString(R.string.page_facebook)))
                .setContentDescription(getString(R.string.share_description_facebook))
                .setContentTitle(getString(R.string.share_title_facebook))
                .build();



        return shareLinkContent;
    }

    @Override
    public void showLoading() {
        // Don't do anything yet
    }

    @Override
    public void hideLoading() {
        // Don't do anything yet
    }

    @Override
    public void openWebView(String token) {
        // Don't do anything yet
    }

    @Override
    public void errorTogetToken() {
        Toast.makeText(this,getString(R.string.erro_generic),Toast.LENGTH_LONG).show();
    }

    @Override
    public void sucessToSave() {
        SharedPreferenceManager.saveState(PostOrPayActivity.this, StatesOfFlowApplication.REPORT);
        Intent i= new Intent(this,DeepLinkActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }
}

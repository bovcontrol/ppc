package br.com.bovcontrol.aliancaterra.ppc3_android.domains.models;

import android.location.Location;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by fernanda on 23/10/16.
 */

public class SlaugtherHouses {
    private String email;
    private String name;
    private String point;
    private float distance;

    public SlaugtherHouses(){

    }
    public SlaugtherHouses(String email, String name, String point) {
        this.email = email;
        this.name = name;
        this.point = point;

    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }



    public void setDistancebetweenTwoPointsinKm(Location farmlocation){
        Location location= new Location("location");
        location=getLocation(point,location);
        if(location!=null)
            distance=farmlocation.distanceTo(location) / 1000; // in km
        else {
            Log.d("ERRROR",""+name);
            distance = 0;
        }
    }

    public static Location getLocation(String point,Location location){
        if(!TextUtils.isEmpty(point)){
            String[] split=point.split(",");
            location.setLatitude(Double.parseDouble(split[1]));
            location.setLongitude(Double.parseDouble(split[0]));
            return location;
        }
        return null;
    }
}

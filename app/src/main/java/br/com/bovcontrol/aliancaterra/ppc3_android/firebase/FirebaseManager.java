package br.com.bovcontrol.aliancaterra.ppc3_android.firebase;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import br.com.bovcontrol.aliancaterra.ppc3_android.Util.GsonHelper;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.DetailFarm;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.LatLng;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Position;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.SlaugtherHouses;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl.UserRepositoryImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.CupomPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.SlaugtherhousesPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.CertificateActivity;

/**
 * Created by ricardoogliari on 10/1/16.
 */

public class FirebaseManager {

    public static void saveuser(final User user, final String id, final Context context){
        DatabaseReference ref = MainApplication.myRef.child("users").child(id);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() == null){
                    finallySaveUser(user, id, context);
                } else {
                    finallySaveUser(snapshot.getValue(User.class), id, context);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("terra", "onCancelled refrefrefrefrefref: " + user);
                finallySaveUser(user, id, context);
            }
        });
    }

    public static void goDbScreen(final String id, final Context context){
        DatabaseReference ref = MainApplication.myRef.child("users").child(id);
        SharedPreferenceManager.saveString(context, id);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() != null){
                    User user = snapshot.getValue(User.class);
                    if(!TextUtils.isEmpty(user.sessionName))
                        SharedPreferenceManager.saveState(context, StatesOfFlowApplication.valueOf(user.sessionName));
                    if(!TextUtils.isEmpty(user.sessionFarm))
                        SessionManager.saveToPrefs(context, SessionManager.PREFS_FARM_UNIQUE_ID,user.sessionFarm);
                    saveUserSession(user, context);
                    StatesOfFlowApplication.showCurrentScreen(SharedPreferenceManager.getState(context), context);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }




    private static void finallySaveUser(User user, String id, Context context){
        Log.e("terra", "finallySaveUser: "+ user.name);
        Log.e("terra", "finallySaveUser: "+ user.sessionName);
        MainApplication.myRef.child("users").child(id).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    //TODO: Tratar erro
                }
            }
        });
        SharedPreferenceManager.saveString(context, id);
        if (user.sessionName == null || user.sessionName.equals(StatesOfFlowApplication.LOGGED.getName()) ||
                user.sessionName.equals(StatesOfFlowApplication.LOGOFF.getName())) {
            Log.e("terra", "finallySaveUser status logged");
            SharedPreferenceManager.saveState(context, StatesOfFlowApplication.LOGGED);
        } else {
            Log.e("terra", "finallySaveUser status " + user.sessionName);
            SharedPreferenceManager.saveState(context, StatesOfFlowApplication.valueOf(user.sessionName));
        }
        if(!TextUtils.isEmpty(user.sessionFarm))
            SessionManager.saveToPrefs(context, SessionManager.PREFS_FARM_UNIQUE_ID,user.sessionFarm);
        StatesOfFlowApplication.showCurrentScreen(SharedPreferenceManager.getState(context), context);
        saveUserSession(user, context);
    }

    private static void finallySaveUserOnlyLogoff(User user, String id, Context context){
        MainApplication.myRef.child("users").child(id).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    //TODO: Tratar erro
                }
            }
        });
        SharedPreferenceManager.saveString(context, id);
        SharedPreferenceManager.saveState(context, StatesOfFlowApplication.LOGOFF);
        StatesOfFlowApplication.showCurrentScreen(SharedPreferenceManager.getState(context), context);
        saveUserSession(user, context);
    }
    private static void changeScreen(User user, String id, Context context){

        saveUserSession(user, context);
    }

    public static void saveUserSession(User user, Context context) {
        GsonHelper<User> gsonHelper=new GsonHelper();
        String json=gsonHelper.toJson(user);
        SessionManager.saveToPrefs(context,SessionManager.PREFS_USER_INFORMATION,json);
    }

    public static void saveDetailFarm(DetailFarm detailFarm, String id, final FirebaseListener listener,Context context,boolean edit){
        User user = getUser(context);
        user.sessionName = StatesOfFlowApplication.INFOS_ABOUT_FARM.getName();
        if(!edit) {
            user.setFarm(detailFarm, context);
        }else{
            String farmId=SessionManager.getFromPrefs(context,SessionManager.PREFS_FARM_UNIQUE_ID,"");
            user.UpdateFarm(farmId,detailFarm);
        }
        MainApplication.myRef.child("users").child(id).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    //TODO: Tratar erro
                    Log.e("ERROR"," "+databaseError.getMessage());
                } else {
                    listener.successAction();
                }
            }
        });
        saveUserSession(user, context);
    }

    public static void saveFarmLocation(LatLng latLng, String idUser,final FirebaseListener firebaseListener,Context context) {
        User user = getUser(context);
        String unique_id_farm=SessionManager.getFromPrefs(context,SessionManager.PREFS_FARM_UNIQUE_ID,"");
        DetailFarm detailFarm=user.farms.get(unique_id_farm);
        detailFarm.setPointPosition(new Position(latLng));
        user.farms.put(unique_id_farm,detailFarm);
        MainApplication.myRef.child("users").child(idUser).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    //TODO: Tratar erro
                } else {
                    firebaseListener.successAction();
                }
            }
        });
        saveUserSession(user, context);
    }


    public static void updateUser(User user, String idUser,final FirebaseListener firebaseListener,Context context) {
        if(user.farms.size()==0){
            user.sessionName = StatesOfFlowApplication.LOGGED.getName();
            SharedPreferenceManager.saveState(context,StatesOfFlowApplication.LOGGED);
        }
        MainApplication.myRef.child("users").child(idUser).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    //TODO: Tratar erro
                } else {
                    firebaseListener.successAction();
                }
            }
        });
        saveUserSession(user, context);

    }

    public static User getUser(Context context) {
        GsonHelper<User> userGsonHelper = new GsonHelper();
        return userGsonHelper.fromJson(SessionManager.getFromPrefs(context, SessionManager.PREFS_USER_INFORMATION,""),User.class);
    }

    public static Report getReport(Context context) {
        GsonHelper<Report> GsonHelper = new GsonHelper();
        return GsonHelper.fromJson(SessionManager.getFromPrefs(context, SessionManager.REPORT_INFORMATION,""),Report.class);
    }


    public static void saveClickRegister(String feature,String userID) {
        MainApplication.myRef.child("newfeature").child(userID).setValue(feature, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    //TODO: Tratar erro
                }
            }
        });
    }

    public static void sessionUserSave(Context ctx, String idUser, String type){
        User user = getUser(ctx);
        user.setSession(type);
        finallySaveUser(user, idUser, ctx);
    }


    public static void sessionUserSaveOnlyLogoff(Context ctx, String idUser, String type){
        User user = getUser(ctx);
        user.setSession(type);
        finallySaveUserOnlyLogoff(user, idUser, ctx);
    }

    public static DetailFarm getFarm(Context context) {
        User user = getUser(context);
        String unique_id_farm=SessionManager.getFromPrefs(context,SessionManager.PREFS_FARM_UNIQUE_ID,"");
        DetailFarm detailFarm=user.farms.get(unique_id_farm);
        return detailFarm;
    }
    public static void  getSlaugtherHouses(final SlaugtherhousesPresenter presenter){
        final List<SlaugtherHouses>[] list = null;
        DatabaseReference ref = MainApplication.myRef.child("slaugtherhouses");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() != null){
                    GenericTypeIndicator<HashMap<String,SlaugtherHouses>> t = new GenericTypeIndicator<HashMap<String,SlaugtherHouses>>() {};
                    presenter.setList(snapshot.getValue(t));

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR","EROOR FIREBASE");

            }
        });

    }

    public static  void saveRequestPromocode(String slaugtherhouses,Context context,final FirebaseListener firebaseListener,String idUser ){
        User user = getUser(context);
        String unique_id_farm=SessionManager.getFromPrefs(context,SessionManager.PREFS_FARM_UNIQUE_ID,"");
        DetailFarm detailFarm=user.farms.get(unique_id_farm);
        detailFarm.request_promocode=slaugtherhouses;
        MainApplication.myRef.child("users").child(idUser).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    //TODO: Tratar erro
                } else {
                    firebaseListener.successAction();
                }
            }
        });
        saveUserSession(user, context);

    }
    public static void saveValidatePromocode(String promocode,Context context,final FirebaseListener firebaseListener,String idUser ){
        User user = getUser(context);
        String unique_id_farm=SessionManager.getFromPrefs(context,SessionManager.PREFS_FARM_UNIQUE_ID,"");
        DetailFarm detailFarm=user.farms.get(unique_id_farm);
        detailFarm.validate_promocode=promocode;
        MainApplication.myRef.child("users").child(idUser).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    //TODO: Tratar erro
                } else {
                    firebaseListener.successAction();
                }
            }
        });
        saveUserSession(user, context);

    }

    public static void getCupomValue(String cupom, String idUser, String farmId, final FirebaseListener firebaseListener) {
        DatabaseReference ref = MainApplication.myRef.child("reports").child(idUser).child(farmId).child("promocodes").child(cupom);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() != null){
                    String retorno=snapshot.getValue().toString();
                    if(retorno.equals("true")){
                        firebaseListener.successAction();
                    }
                    else{
                        firebaseListener.errorAction();
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR","EROOR FIREBASE");
                firebaseListener.errorAction();
            }
        });
    }

    public static void saveRequestPayment(String payment_required,Context context,final FirebaseListener firebaseListener,String idUser){
        User user = getUser(context);
        String unique_id_farm=SessionManager.getFromPrefs(context,SessionManager.PREFS_FARM_UNIQUE_ID,"");
        DetailFarm detailFarm=user.farms.get(unique_id_farm);
        detailFarm.payment_required=payment_required;
        MainApplication.myRef.child("users").child(idUser).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    //TODO: Tratar erro
                } else {
                    firebaseListener.successAction();
                }
            }
        });
        saveUserSession(user, context);

    }


    public static void saveSession(StatesOfFlowApplication state, final Context context, final String id) {
        final User user=getUser(context);
        user.sessionName=state.getName();
        DatabaseReference ref = MainApplication.myRef.child("users").child(id);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                saveUserSession(user, context);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("terra", "onCancelled refrefrefrefrefref: " + user);
                saveUserSession(user, context);
            }
        });

    }
}

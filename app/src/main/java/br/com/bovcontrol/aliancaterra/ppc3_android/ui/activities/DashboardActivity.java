package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.MobileAnalytics;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.UserInformations;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.UserRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.CertificatePresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.UserPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.CertificatePresenterImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.UserPresenterImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.CertificateView;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.UserView;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.fragments.TabFase1;
import butterknife.BindView;
import butterknife.ButterKnife;


public class DashboardActivity extends GenerateCertificate implements CertificateView, UserView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fragment)
    LinearLayout fragment;

    @BindView(R.id.tabs)
    TabLayout tabs;

    @BindView(R.id.clDash)
    CoordinatorLayout clDash;

    CertificatePresenter presenter;

    UserPresenter user_presenter;
    ProgressDialog mDialog;
    ProgressDialog mDialogInitial;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
            ft.add(R.id.fragment, new TabFase1());
            ft.commit();
        }

        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    Fase1();
                } else {
                    String fase_you_want = "fase" + (tab.getPosition() + 1);
                    createDialog(fase_you_want);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    Fase1();
                } else {
                    String fase_you_want = "fase" + (tab.getPosition() + 1);
                    createDialog(fase_you_want);
                }
            }
        });

        presenter = new CertificatePresenterImpl();
        user_presenter= new UserPresenterImpl(this,this);
        presenter.obtainView(this);
        String idUser = SharedPreferenceManager.getId(this);
        String idFarm= SessionManager.getFromPrefs(this,SessionManager.PREFS_FARM_UNIQUE_ID,"");
        presenter.loadData(idUser,idFarm);


    }




    private void Fase1() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        ft.add(R.id.fragment, new TabFase1());
        ft.commit();
    }

    private void createDialog(final String faseclick) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.PPC3AlerStyle);
        builder.setTitle(getString(R.string.know_new_features_title));
        builder.setMessage(getString(R.string.know_new_features_msg));
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mobileAnalytics.AddEvent(DashboardActivity.this,DashboardActivity.this,"wantknowphase"+faseclick);
                user_presenter.saveIntention(faseclick);

            }
        });
        builder.setNegativeButton(getString(R.string.cancel), null);
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {

//           GenerateCertificatePermissionsDispatcher.writeFileWithCheck(this);
//            presenter.generatePdf(report);
            clickToGenerateReport();
        }
        if (item.getItemId() == R.id.miLogoff) {

            logoff();
            return true;
        }else if (item.getItemId() == R.id.miMoreApps) {
            openBovcontrolOnboarding();
            return true;
        } else {
            return false;
        }


    }


    @Override
    void writebypresenter() {
        presenter.generatePdf(report);
    }

    @Override
    String getActivityName() {
        return "DASH";
    }


    @Override
    public void showData(Report report) {
        this.report = report;
        verifyExtra();

    }

    @Override
    public void changeBackgound(boolean isContained) {

    }

    @Override
    public void showLoading() {
        if(mDialog==null) {
            mDialog = new ProgressDialog(this);
            mDialog.setTitle(getString(R.string.gen_please_wait));
            mDialog.setMessage(getString(R.string.gen_pdf_file));
            mDialog.setIndeterminate(false);
            mDialog.setCancelable(false);
            mDialog.show();
        }




    }

    @Override
    public void hideLoading() {
        if((mDialog != null) && mDialog.isShowing()){
            mDialog.dismiss();
            mDialog = null;
        }


    }

    @Override
    public void showErrorMessage(String message) {
        Snackbar.make(clDash,getString(R.string.erro_generic),Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showWihoutNetwork() {

    }

    @Override
    public void sucessToSaveIntention() {
        Snackbar.make(clDash,getString(R.string.thank_you_to_participate),Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void errorToSaveIntention() {
        Snackbar.make(clDash,getString(R.string.erro_generic),Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}

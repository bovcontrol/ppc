package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.UserRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl.UserRepositoryImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.RecoverPasswordView;

/**
 * Created by fernanda on 14/11/16.
 */

public class RecoverPresenter {
    UserRepository repository;
    RecoverPasswordView passwordView;
    public RecoverPresenter(RecoverPasswordView passwordView){
        this.passwordView=passwordView;
        repository= new UserRepositoryImpl();
    }
    public void sendRecoverPassword(String email){
        FirebaseListener listener= new FirebaseListener() {
            @Override
            public void successAction() {
                SucessSendRecover();
            }

            @Override
            public void errorAction() {
                FailSendRecover();
            }
        };

        repository.sendEmailRecoverPassword(email,listener);
    }

    public void SucessSendRecover(){
        passwordView.sucessTorecover();
    }
    public void FailSendRecover(){
        passwordView.failToRecover();
    }
}

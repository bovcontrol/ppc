package br.com.bovcontrol.aliancaterra.ppc3_android.injectors.modules;

import javax.inject.Singleton;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.FarmRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl.FarmRepositoryImpl;
import dagger.Module;
import dagger.Provides;

/**
 * Created by fernanda on 27/09/16.
 */
@Module
public class FarmRepositoryModule {
    @Provides
    @Singleton
    public FarmRepository provideFarmRepository() {
        return new FarmRepositoryImpl();
    }




}

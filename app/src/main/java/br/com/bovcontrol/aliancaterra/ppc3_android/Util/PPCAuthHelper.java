package br.com.bovcontrol.aliancaterra.ppc3_android.Util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Bundle;

/**
 * Created by fernanda on 14/07/17.
 */

public class PPCAuthHelper {
    public final String account_type="com.bovcontrol.aliancaterra.ppc";
    public void addAccount(String mUsername, String mPassword,Context context){
        final AccountManager am = AccountManager.get(context);
        final Account account = new Account(mUsername, account_type);
        final Bundle extraData = new Bundle();
        extraData.putString("token", mPassword);
        extraData.putString("email", mUsername);

        am.addAccountExplicitly(account, mPassword, null);
        am.setAuthToken(account, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS, mPassword);
        am.addAccount(account_type, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS,null,extraData,null,null,null);
    }
}

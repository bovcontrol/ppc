package br.com.bovcontrol.aliancaterra.ppc3_android.domains.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nic on 04/10/16.
 */

public class Report {

    public String deforestation;
    public String embargo;
    public String forced_labor;
    public String proximity_indigenous_lands;
    public String proximity_protected_areas;
    public String paid;
    public String payment_session;

    //region Computed
    public boolean isCompliance()
    {
        return (
                !this.deforestation.isEmpty() &&
                !this.embargo.isEmpty() &&
                !this.forced_labor.isEmpty() &&
                !this.proximity_indigenous_lands.isEmpty() &&
                !this.proximity_protected_areas.isEmpty()
        );
    }


    public String getComplianceMessage()
    {
        return this.isCompliance() ? "Tudo certo" : "Não certo";
    }


    public String getName()
    {
        return "Relatório Aliança da Terra";
    }
    //endregion

    //region Getters & Setter

    public String getDeforestation() {
        return deforestation;
    }

    public void setDeforestation(String deforestation) {
        this.deforestation = deforestation;
    }

    public String getEmbargo() {
        return embargo;
    }

    public void setEmbargo(String embargo) {
        this.embargo = embargo;
    }

    public String getForced_labor() {
        return forced_labor;
    }

    public void setForced_labor(String forced_labor) {
        this.forced_labor = forced_labor;
    }

    public String getProximity_indigenous_lands() {
        return proximity_indigenous_lands;
    }

    public void setProximity_indigenous_lands(String proximity_indigenous_lands) {
        this.proximity_indigenous_lands = proximity_indigenous_lands;
    }

    public String getProximity_protected_areas() {
        return proximity_protected_areas;
    }

    public void setProximity_protected_areas(String proximity_protected_areas) {
        this.proximity_protected_areas = proximity_protected_areas;
    }

    //endregion

    //region Constructor
    public Report(){

    }
    public Report(String deforestation, String embargo, String forced_labor, String proximity_indigenous_lands, String proximity_protected_areas) {
        this.deforestation = deforestation;
        this.embargo = embargo;
        this.forced_labor = forced_labor;
        this.proximity_indigenous_lands = proximity_indigenous_lands;
        this.proximity_protected_areas = proximity_protected_areas;
    }

    public List<ReportItems> getItensCertificate() {

        List<ReportItems> itemsCertificates = new ArrayList<>();

        itemsCertificates.add(
                new ReportItems("report_items_deforestation", !this.deforestation.isEmpty(), this.deforestation, this.deforestation));
        itemsCertificates.add(
                new ReportItems("report_items_embargo", !this.embargo.isEmpty(), this.embargo, this.embargo));
        itemsCertificates.add(
                new ReportItems("report_items_forced_labor", !this.forced_labor.isEmpty(), this.forced_labor, this.forced_labor));
        itemsCertificates.add(
                new ReportItems("report_items_proximity_indigenous_lands", !this.proximity_indigenous_lands.isEmpty(), this.proximity_indigenous_lands, this.proximity_indigenous_lands));
        itemsCertificates.add(
                new ReportItems("report_items_proximity_protected_areas", !this.proximity_protected_areas.isEmpty(), this.proximity_protected_areas, this.proximity_protected_areas));

        return itemsCertificates;
    }

    public void add(String key, String value) {
        switch (key){
           case "deforestation":
               deforestation=value;break;
           case "embargo":
               embargo=value;break;
           case "forced_labor":
               forced_labor=value;break;
            case "proximity_indigenous_lands":
                proximity_indigenous_lands=value;break;
             case "proximity_protected_areas":
                 proximity_protected_areas=value;break;

        }
    }
    //endregion
}

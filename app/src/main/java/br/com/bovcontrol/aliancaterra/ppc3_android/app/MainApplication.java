package br.com.bovcontrol.aliancaterra.ppc3_android.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.bovcontrol.signon.widget.BovControlApplication;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.Mixpanel;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.injectors.Component.CertificateComponent;
import br.com.bovcontrol.aliancaterra.ppc3_android.injectors.Component.DaggerCertificateComponent;
import br.com.bovcontrol.aliancaterra.ppc3_android.injectors.Component.DaggerFarmRepositoryComponent;
import br.com.bovcontrol.aliancaterra.ppc3_android.injectors.Component.FarmRepositoryComponent;
import br.com.bovcontrol.aliancaterra.ppc3_android.injectors.modules.CertificateModule;
import br.com.bovcontrol.aliancaterra.ppc3_android.injectors.modules.FarmRepositoryModule;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by fernanda on 23/09/16.
 */

public class MainApplication extends BovControlApplication {
    private static MainApplication sInstance;
    private Tracker mTracker;
    private FarmRepositoryComponent farmRepositoryComponent;
    private CertificateComponent CertificateComponent;
    public static String MIXPANEL_TOKEN;
    public static MixpanelAPI mixpanel;
    private static FirebaseAnalytics mFirebaseAnalytics;
    public static FirebaseDatabase database;
    public static DatabaseReference myRef;
    static FirebaseApp firebaseInstance;
    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        sInstance = this;
        DaggerInjectionsInstance();

        firebaseInstance = FirebaseApp.initializeApp(getApplicationContext());

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        initMixpanel();

    }

    private void initMixpanel() {
        MIXPANEL_TOKEN = getString(R.string.mixpanel);
        mixpanel = MixpanelAPI.getInstance(this, MainApplication.MIXPANEL_TOKEN);
        try {
            Mixpanel.adicionarMixpanel(null, getApplicationContext(), "InitializeApp");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics = GoogleAnalytics.getInstance(this);
            analytics.setLocalDispatchPeriod(1800);
            mTracker = analytics.newTracker(getString(R.string.ga_property_id));
            mTracker.enableExceptionReporting(true);
            mTracker.enableAdvertisingIdCollection(true);
            mTracker.enableAutoActivityTracking(true);
        }
        return mTracker;
    }
    private void DaggerInjectionsInstance() {
        farmRepositoryComponent= DaggerFarmRepositoryComponent.create();
        farmRepositoryComponent=DaggerFarmRepositoryComponent.builder().farmRepositoryModule(new FarmRepositoryModule()).build();
        CertificateComponent= DaggerCertificateComponent.create();
        farmRepositoryComponent=DaggerFarmRepositoryComponent.builder().farmRepositoryModule(new FarmRepositoryModule()).build();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static MainApplication getInstance() {
        return sInstance;
    }

    public FarmRepositoryComponent farmRepositoryComponent() {
        return farmRepositoryComponent;
    }
    public CertificateComponent certificateComponent() {
        return CertificateComponent;
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import javax.inject.Inject;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.UserInformations;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.CertificatePresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.CertificateView;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.adapters.ReportItemsAdapter;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.DividerItemDecoration;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StatusBarUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CertificateActivity extends GenerateCertificate implements CertificateView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.send_report)
    Button shareCertificate;
    @BindView(R.id.content_certificate)
    LinearLayout contentCertificate;
    @Inject
    CertificatePresenter presenter;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.clcertificate)
    CoordinatorLayout clcertificate;
    @BindView(R.id.llReportedContained)
    LinearLayout llReportedContained;
    @BindView(R.id.llNothingContained)
    LinearLayout llNothingContained;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.view2)
    View view2;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_certificate);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ((MainApplication) getApplication()).certificateComponent().inject(this);
        presenter.obtainView(this);
        String idUser = SharedPreferenceManager.getId(this);
        String idFarm=SessionManager.getFromPrefs(this,SessionManager.PREFS_FARM_UNIQUE_ID,"");
        presenter.loadData(idUser,idFarm);
        SessionManager.saveToPrefs(this,SessionManager.PREFS_ACTIVITY_SHOW_REPORT,getActivityName());
        FirebaseManager.saveSession(StatesOfFlowApplication.REPORT,this,SharedPreferenceManager.getId(this));
//        FirebaseManager.sessionUserSave(this, SharedPreferenceManager.getId(this), StatesOfFlowApplication.REPORT.getName());

    }

    @Override
    protected void onResume() {
        super.onResume();
        String report=SessionManager.getFromPrefs(this,"state","");
        if(report.equals(StatesOfFlowApplication.REPORT.getName())){
            Intent i= new Intent(this,DashboardActivity.class);
            startActivity(i);
            finish();
        }else{
            SharedPreferenceManager.saveState(this, StatesOfFlowApplication.REPORT);
        }
    }

    @OnClick(R.id.send_report)
    public void onClick() {
        mobileAnalytics.AddEvent(this,this,"buttonshare");
       clickToGenerateReport();

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showErrorMessage(String message) {

    }

    @Override
    public void showWihoutNetwork() {

    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.alertExit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public void showData(Report report) {
        this.report = report;
        verifyExtra();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recycler.setLayoutManager(linearLayoutManager);

        Drawable dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider);

        recycler.addItemDecoration(new DividerItemDecoration(dividerDrawable));

        recycler.setAdapter(new ReportItemsAdapter(report.getItensCertificate(), this));
        presenter.isContained(report.getItensCertificate());


    }


    @Override
    public void changeBackgound(boolean isContained) {
        int color = 0;
        int color_dark = 0;
        if (isContained) {
            color = getResources().getColor(R.color.colorAccent);
            color_dark = getResources().getColor(R.color.brown);
        } else {
            color = getResources().getColor(R.color.colorPrimary);
            color_dark = getResources().getColor(R.color.colorPrimaryDark);
        }
        clcertificate.setBackgroundColor(color);
        toolbar.setBackgroundColor(color);
        Window window = getWindow();
        StatusBarUtil.changeStatusBar(window,color_dark);
        changeTextAndIcon(isContained);
    }

    private void changeTextAndIcon(boolean isContained) {
        if(isContained)
            llReportedContained.setVisibility(View.VISIBLE);
        else
            llNothingContained.setVisibility(View.VISIBLE);

    }

    @Override
    void writebypresenter() {
        presenter.generatePdf(report);
    }

    @Override
    String getActivityName() {
        return "CERTIFICATE";
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.Util;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import br.com.bovcontrol.aliancaterra.ppc3_android.app.MainApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;

/**
 * Created by fernanda on 30/07/17.
 */

public class Mixpanel {
    public static void addMixPanel(Activity activity, String action){

        try {
            JSONObject json = new JSONObject();
            json.put("Activity", "" +  activity.getClass().getSimpleName().toLowerCase());
            MainApplication.mixpanel.track(action, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public static void addMixPanel(Activity activity, Context context, String action) {
        addMixPanel(activity, context, action, null);
    }

    public static void addMixPanel(Fragment fragment, Context context, String action) {
        addMixPanel(fragment, context, action, null);
    }

    public static void addMixPanel(Fragment fragment, Context context, String action, HashMap<String, String> mixpanelporperty) {
        try {


                JSONObject json = new JSONObject();
                json.put("Activity", "" + fragment.getClass().getSimpleName().toLowerCase() + action);
                MixPanelObject(mixpanelporperty, json);
                MainApplication.mixpanel.track(Mixpanel.getActionMixPanel(fragment, action), json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void addMixPanel(Activity activity, Context context, String action, HashMap<String, String> mixpanelporperty) {
            MainApplication application = (MainApplication) activity.getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName( activity.getClass().getSimpleName().toLowerCase());
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());

          try {
                JSONObject json = new JSONObject();
                json.put("Activity", "" + activity.getClass().getSimpleName().toLowerCase() + action);
                MixPanelObject(mixpanelporperty, json);
                MainApplication.mixpanel.track(Mixpanel.getActionMixPanel(activity, action), json);
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }

    public static void LogOut(Context context){
        Mixpanel.initializeMixPanel(context);
    }

    private static void MixPanelObject(HashMap<String, String> mixpanelporperty, JSONObject json) throws JSONException {
        if (mixpanelporperty != null && (!mixpanelporperty.isEmpty())) {
            for (Map.Entry<String, String> entry : mixpanelporperty.entrySet()) {
                json.put(entry.getKey(), entry.getValue());
            }
        }
    }

    private static String getActionMixPanel(Activity activity, String action) {
        return  activity.getClass().getSimpleName().toLowerCase() + "" + action;
    }

    private static String getActionMixPanel(Fragment fragment, String action) {
        return fragment.getClass().getSimpleName().toLowerCase() + action;
    }

    public static void adicionarMixpanel(Activity activity, Context context, String className) throws JSONException {
        if (activity != null) {
            MainApplication application = (MainApplication) activity.getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName(activity.getClass().getSimpleName().toLowerCase());
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }

        Mixpanel.initializeMixPanel(context);
        try {
            JSONObject json = new JSONObject();
            json.put("Activity", "" + className);
            if (MainApplication.mixpanel != null && !SessionManager.getFromPrefs(context, SessionManager.PREFS_USER_ID_MIXPANEL, "").equals("") || (className.toUpperCase().contains("ERROR"))) {
                MainApplication.mixpanel.track(className, json);
            } else {
                Log.d("TAG_MIXPANEL", "Sem Usuário ID Remoto");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void signupMixPanel(Context context, String entity_people_id) {
        MainApplication.mixpanel.alias(entity_people_id, null);
        MainApplication.mixpanel.getPeople().set("$first_name", SessionManager.getFromPrefs(context,SessionManager.PREF_SAVE_NAME,""));
        MainApplication.mixpanel.getPeople().set("$email", SessionManager.getFromPrefs(context,SessionManager.PREF_SAVE_EMAIL,""));


    }


    public static void initializeMixPanel(Context context) {

        String userPublicId = SessionManager.getFromPrefs(context, SessionManager.PREFS_USER_ID_MIXPANEL, "");

        if (!TextUtils.isEmpty(userPublicId)) {
            MainApplication.mixpanel.identify(userPublicId);
            MainApplication.mixpanel.getPeople().identify(userPublicId);
        }
        if(MainApplication.mixpanel!=null && MainApplication.mixpanel.getPeople()!=null)
            MainApplication.mixpanel.getPeople().initPushHandling("452640920188");

    }
}

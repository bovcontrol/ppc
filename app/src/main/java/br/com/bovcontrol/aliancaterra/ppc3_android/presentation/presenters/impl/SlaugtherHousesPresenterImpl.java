package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl;

import android.content.Context;

import java.util.HashMap;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.SlaugtherHouses;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.SlaugtherhousesPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.SlaugtherHousesView;

/**
 * Created by fernanda on 25/10/16.
 */

public class SlaugtherHousesPresenterImpl implements SlaugtherhousesPresenter {
    SlaugtherHousesView view;


    public SlaugtherHousesPresenterImpl(SlaugtherHousesView view){
        this.view=view;
    }
    @Override
    public void getSlaughters() {
        view.showLoading();
        FirebaseManager.getSlaugtherHouses(this);

    }

    @Override
    public void setList(HashMap<String, SlaugtherHouses> value) {
        view.showList(value);
        view.hideLoading();
    }

    @Override
    public void save(String uuidSlaugther, String idUser, Context context) {
        FirebaseManager.saveRequestPromocode(uuidSlaugther, context, new FirebaseListener() {
            @Override
            public void successAction() {
                successTogenerateCoupon();

            }

            @Override
            public void errorAction() {
                failTogenerateCoupon();
            }
        }, idUser);
    }

    @Override
    public void successTogenerateCoupon() {
        view.hideLoading();
        view.successtogenerateCoupon();
    }

    @Override
    public void failTogenerateCoupon() {
        view.hideLoading();
        view.failtogenerateCoupon();
    }
}

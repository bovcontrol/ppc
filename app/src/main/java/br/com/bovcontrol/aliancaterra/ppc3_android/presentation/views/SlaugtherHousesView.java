package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views;

import java.util.HashMap;
import java.util.List;

import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.SlaugtherHouses;

/**
 * Created by fernanda on 25/10/16.
 */

public interface SlaugtherHousesView {
    void showList(HashMap<String,SlaugtherHouses> slaugtherHousesList);
    void showLoading();
    void hideLoading();
    void successtogenerateCoupon();
    void failtogenerateCoupon();
}

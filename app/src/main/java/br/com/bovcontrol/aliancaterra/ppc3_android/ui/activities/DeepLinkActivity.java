package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;

public class DeepLinkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deep_link);
//        Intent intent = getIntent();
//        String action = intent.getAction();
//        Uri data = intent.getData();
        Intent intentactual=null;
        if("CERTIFICATE".equals(SessionManager.getFromPrefs(this,SessionManager.PREFS_ACTIVITY_SHOW_REPORT,""))){
            intentactual= new Intent(this,CertificateActivity.class);
        }else{
            intentactual= new Intent(this,DashboardActivity.class);
        }
        intentactual.putExtra("GenerateCertificate","yes");
        SharedPreferenceManager.saveState(DeepLinkActivity.this, StatesOfFlowApplication.REPORT);
        SessionManager.saveToPrefs(this,SessionManager.PREFS_ACTIVITY_SHOW_REPORT,"");
        startActivity(intentactual);
        finish();
    }
}

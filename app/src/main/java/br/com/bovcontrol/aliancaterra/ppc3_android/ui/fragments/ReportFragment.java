package br.com.bovcontrol.aliancaterra.ppc3_android.ui.fragments;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.Util.MobileAnalytics;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.Report;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.ReportItems;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.User;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.CertificatePresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.CertificatePresenterImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.CertificateView;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.ConfirmYourDataActivity;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.DetailFarm;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities.GenerateCertificate;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.adapters.ReportItemsAdapter;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.DividerItemDecoration;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ReportFragment extends BaseFragment implements CertificateView {
    MobileAnalytics mobileAnalytics= new MobileAnalytics();
    String userId;
    List<ReportItems> list;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @Inject
    CertificatePresenter presenter;
    @BindView(R.id.tvreportTitle)
    TextView tvreportTitle;
    @BindView(R.id.tvreportResult)
    TextView tvreportResult;
    @BindView(R.id.containerReport)
    LinearLayout containerReport;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.SpinnerFarm)
    Spinner SpinnerFarm;
    ProgressDialog mDialog;
    @BindView(R.id.progressBarFeed)
    ProgressBar progressBarFeed;
    @BindView(R.id.rlWelcome)
    LinearLayout rlWelcome;
    @BindView(R.id.containerReportSpinner)
    LinearLayout containerReportSpinner;
    @BindView(R.id.containerprogressBar)
    FrameLayout containerprogressBar;
    @BindView(R.id.llcontainer)
    LinearLayout llcontainer;
    public static String Report = "Report";
    @BindView(R.id.remove)
    AppCompatImageView remove;

    public ReportFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public static ReportFragment with(List<ReportItems> reportItemsList) {
        final Bundle args = new Bundle();
        ReportFragment f = new ReportFragment();
        f.setArguments(args);
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        ButterKnife.bind(this, view);
        presenter = new CertificatePresenterImpl();
        presenter.obtainView(this);
        final String idUser = SharedPreferenceManager.getId(getActivity());
        final User user = FirebaseManager.getUser(getActivity());
        final List<String> farmNames = user.getFarmNames();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, farmNames);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        SpinnerFarm.setAdapter(adapter);
        SpinnerFarm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String farmId = user.codeAndNameFarm.get(farmNames.get(position));
                SessionManager.saveToPrefs(getActivity(), SessionManager.PREFS_FARM_UNIQUE_ID, farmId);
                String idFarm = SessionManager.getFromPrefs(getActivity(), SessionManager.PREFS_FARM_UNIQUE_ID, "");
                presenter.loadData(idUser, idFarm);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        final String idFarm = SessionManager.getFromPrefs(getActivity(), SessionManager.PREFS_FARM_UNIQUE_ID, "");
        SpinnerFarm.setSelection(user.getPositionByFarmId(idFarm, farmNames));
        presenter.loadData(idUser, idFarm);
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idFarm = SessionManager.getFromPrefs(getActivity(), SessionManager.PREFS_FARM_UNIQUE_ID, "");
                int position=user.getPositionByFarmId(idFarm, farmNames);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.PPC3AlerStyle);
                builder.setTitle(getString(R.string.remove_farm));
                builder.setMessage(getString(R.string.msg_removefarm)+" "+farmNames.get(position)+" ?");
                builder.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        RemoveFarm(user, farmNames, adapter, idUser);
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel), null);
                builder.show();



            }
        });
        return view;
    }

    private void RemoveFarm(User user, List<String> farmNames, ArrayAdapter<String> adapter, String idUser) {
        String idFarm = SessionManager.getFromPrefs(getActivity(), SessionManager.PREFS_FARM_UNIQUE_ID, "");
        int position=user.getPositionByFarmId(idFarm, farmNames);
        presenter.removeAtualFarm(getActivity());
        if(farmNames.size()==1){
            Intent intent = new Intent(getActivity(), DetailFarm.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{

            try {
                farmNames.remove(farmNames.get(position));
                adapter.notifyDataSetChanged();
                String farmId = user.codeAndNameFarm.get(farmNames.get(0));
                SessionManager.saveToPrefs(getActivity(), SessionManager.PREFS_FARM_UNIQUE_ID, farmId);
                presenter.loadData(idUser, idFarm);
            }catch(Exception e){
                Intent intent = new Intent(getActivity(), DetailFarm.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

        }
    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public Application getApplication() {
        return getActivity().getApplication();
    }

    @Override
    public void showData(Report report) {
        if (report != null && ((GenerateCertificate) getActivity()).report != null) {
            ((GenerateCertificate) getActivity()).report = report;
            this.list = report.getItensCertificate();
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);
            try {
                Drawable dividerDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.divider_black);
                recyclerView.addItemDecoration(new DividerItemDecoration(dividerDrawable));
            } catch (Exception e) {

            }
            recyclerView.setAdapter(new ReportItemsAdapter(list, getActivity()));
            presenter.isContained(list);
        } else {
            Toast.makeText(getActivity(), getString(R.string.erro_generic), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void sucessPdfGenerator(File path) {

    }

    @Override
    public void changeBackgound(boolean isContained) {
        int color = 0;
        String situation = "";
        if (isContained) {
            color = getResources().getColor(R.color.colorAccent);
            situation = getString(R.string.irregular);
            containerReport.setBackgroundColor(getResources().getColor(R.color.maroon5));

        } else {
            color = getResources().getColor(R.color.colorPrimary);
            situation = getString(R.string.regular);
            containerReport.setBackgroundColor(getResources().getColor(R.color.green5Alpha));

        }
        tvreportResult.setTextColor(color);
        tvreportTitle.setTextColor(color);
        tvreportResult.setText(situation);
        recyclerView.setAdapter(new ReportItemsAdapter(list, getActivity(), color));


    }

    @Override
    public void onErrorPdfGenerated() {

    }

    @Override
    public void showLoading() {
        containerprogressBar.setVisibility(View.VISIBLE);
        llcontainer.setVisibility(View.GONE);

    }

    @Override
    public void hideLoading() {
        containerprogressBar.setVisibility(View.GONE);
        llcontainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorMessage(String message) {

    }

    @Override
    public void showWihoutNetwork() {

    }


    @OnClick(R.id.fab)
    public void onClick() {
        mobileAnalytics.AddEvent(getActivity(),getActivity(),"button.newFarm");
        Intent intent = new Intent(getActivity(), DetailFarm.class);
        intent.putExtra(Report, "yes");
        getActivity().startActivity(intent);
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.StatesOfFlowApplication;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.PaymentPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl.PaymentPresenterImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.PaymentView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentActivity extends BaseActivity implements PaymentView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBarPayment)
    ProgressBar progressBarPayment;
    PaymentPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        createToolbarBackArrow();
        presenter= new PaymentPresenterImpl(this,this);
        presenter.saveRequest_payment("true");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }

    @Override
    public void showLoading() {
        progressBarPayment.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBarPayment.setVisibility(View.GONE);
    }

    @Override
    public void openWebView(String token) {
        String url = getString(R.string.payment)+token;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        SharedPreferenceManager.saveState(PaymentActivity.this, StatesOfFlowApplication.REPORT);
        finish();
    }

    @Override
    public void errorTogetToken() {
        progressBarPayment.setVisibility(View.GONE);

    }

    @Override
    public void sucessToSave() {
        //Don't do anything yet
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

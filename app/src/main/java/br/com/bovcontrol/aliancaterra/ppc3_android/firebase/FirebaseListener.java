package br.com.bovcontrol.aliancaterra.ppc3_android.firebase;

/**
 * Created by ricardoogliari on 10/3/16.
 */

public interface FirebaseListener {

    public void successAction();
    public void errorAction();

}

package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl;

import android.content.Context;

import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.PaymentRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl.PaymentRepositoryImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.firebase.FirebaseListener;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.PaymentPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.PaymentView;

/**
 * Created by fernanda on 26/10/16.
 */

public class PaymentPresenterImpl implements PaymentPresenter {
    PaymentView view;
    PaymentRepository repository;
    Context context;
    public PaymentPresenterImpl(PaymentView view,Context context){
        this.view=view;
        repository= new PaymentRepositoryImpl(context,this);
        this.context=context;
    }

    @Override
    public void saveRequest_payment(final String required) {
        view.showLoading();
        repository.saveRequestPayment(required,new FirebaseListener() {
            @Override
            public void successAction() {
                if("true".equals(required))
                    repository.getToken(SharedPreferenceManager.getId(context), SessionManager.getFromPrefs(context,SessionManager.PREFS_FARM_UNIQUE_ID,""));
                else
                    view.sucessToSave();
            }

            @Override
            public void errorAction() {
                view.errorTogetToken();
            }
        }, SharedPreferenceManager.getId(context));

    }

    @Override
    public void successGetToken(String token) {
        view.openWebView(token);
        view.hideLoading();
    }

    @Override
    public void errorGetToken(String error) {
        view.errorTogetToken();
    }


}

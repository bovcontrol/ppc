package br.com.bovcontrol.aliancaterra.ppc3_android.ui.util;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;

/**
 * Created by fernanda on 28/09/16.
 */

public class FileUtils {


    public static File getFileDirectory(Context context)  {

        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File f = new File(path, "vem" +".jpg");
        try {
            // Make sure the Pictures directory exists.
            path.mkdirs();

            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }
}

package br.com.bovcontrol.aliancaterra.ppc3_android.ui.util;

import android.content.Context;
import android.text.format.DateFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;


/**
 * Created by fernanda on 26/09/16.
 */
public class DateHelper {

    public static String getDate(Context mcontext, int Id) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(mcontext.getString(R.string.dateformate));
        SimpleDateFormat timeFormat = new SimpleDateFormat(mcontext.getString(R.string.timeformate));
        //get current date time with Date()
        Date date = new Date();
        String dateString=dateFormat.format(date);
        String timeString=timeFormat.format(date);

        String text = String.format(mcontext.getString(Id), dateString);
        text+=" "+timeString;

        return text;

    }
}

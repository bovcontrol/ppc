package br.com.bovcontrol.aliancaterra.ppc3_android.ui.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.database.SharedPreferenceManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StatusBarUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;


public class ChoiceAboutLocationActivity extends BaseGpsActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textViewTile)
    TextView textViewTile;
    @BindView(R.id.textViewNothingContained)
    TextView textViewNothingContained;
    @BindView(R.id.my_location)
    Button myLocation;
    @BindView(R.id.textviewContained)
    TextView textviewContained;
    @BindView(R.id.see_map)
    Button seeMap;
    @BindView(R.id.activity_progress_wait_data)
    LinearLayout activityProgressWaitData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_about_location);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceManager.saveBoolean(ChoiceAboutLocationActivity.this, "from_choice", true);

                onBackPressed();
            }
        });
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.only_logoff, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.miLogoff) {
            logoff();
            return true;
        } else if (item.getItemId() == R.id.miMoreApps){
            openBovcontrolOnboarding();
            return true;
        } else {
            SharedPreferenceManager.saveBoolean(ChoiceAboutLocationActivity.this, "from_choice", true);
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick({R.id.my_location, R.id.see_map})
    public void onClick(View view) {
         type="my_location";
        switch (view.getId()) {
            case R.id.my_location:
                mobileAnalytics.AddEvent(this,this,"mylocation");
                type = MapsActivity.mylocation;
                break;
            case R.id.see_map:
                mobileAnalytics.AddEvent(this,this,"seemap");
                type = MapsActivity.see_map;
                break;
        }
        BaseGpsActivityPermissionsDispatcher.needPermissionTodoNextActionWithCheck(this, type);
    }


    @Override
    void ActionAfterLocationPermitionIsAccept(String type) {

        if (type.equals(MapsActivity.mylocation) && (!gps.canGetLocation()))
            GPSMessageRequired();
        else {
            Intent intent = new Intent(this, MapsActivity.class);
            intent.putExtra("type", type);
            startActivity(intent);
        }

    }


}

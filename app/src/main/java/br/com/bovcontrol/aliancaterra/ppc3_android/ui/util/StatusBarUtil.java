package br.com.bovcontrol.aliancaterra.ppc3_android.ui.util;

import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by fernanda on 30/09/16.
 */

public class StatusBarUtil {

    public static void changeStatusBar(Window window,int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

}

package br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.impl;

import android.content.Context;

import com.google.gson.Gson;

import br.com.bovcontrol.aliancaterra.ppc3_android.Util.GsonHelper;
import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.LatLng;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.FarmRepository;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.repositories.impl.FarmRepositoryImpl;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.BasePresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.CertificatePresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.presenters.FarmPresenter;
import br.com.bovcontrol.aliancaterra.ppc3_android.presentation.views.FarmView;

/**
 * Created by fernanda on 30/09/16.
 */

public class FarmPresenterImpl implements FarmPresenter {

    FarmView farmView;
    FarmRepository farmRepository;
    Context context;

    public FarmPresenterImpl(FarmView farmView, Context context){
        this.farmView=farmView;
        farmRepository= new FarmRepositoryImpl();
        this.context=context;
    }

    @Override
    public void getLtnLng() {
        GsonHelper<LatLng> gsonHelper= new GsonHelper();
//        if(SessionManager.getFromPrefs(context,SessionManager.PREFS_FARM_LAT_LNG,"0").equals("0")) {
            LatLng latLng=farmRepository.getLatLng(context);

//            SessionManager.saveToPrefs(context,SessionManager.PREFS_FARM_LAT_LNG,gsonHelper.toJson(latLng));
//        }
//        LatLng latLng =gsonHelper.fromJson(SessionManager.getFromPrefs(context,SessionManager.PREFS_FARM_LAT_LNG,"0"), br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.LatLng.class);
        farmView.setMarker(latLng.getLat(),latLng.getLng());


    }
}

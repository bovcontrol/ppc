package br.com.bovcontrol.aliancaterra.ppc3_android.domains.models;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;

/**
 * Created by fernanda on 26/09/16.
 */

public class UserInformations {
    private long id;
    private String name;
    private String email;
    private String farmName;
    private String cnpjcpf;
    private String partner;
    private String coordinates;
    private List<ReportItems> itensCertificate= new ArrayList<>();
    private boolean forcedLabor;
    private boolean deflorestation;
    private boolean embargo;
    private boolean ProximityToIndigenousLands;
    private boolean ProximityToProtectedareas;

    public static String isContained(Context context, boolean test){
        if(test)
            return context.getResources().getString(R.string.contained);
        else
            return context.getResources().getString(R.string.nothingcontained);
    }

    public List<ReportItems> getItensCertificate() {
        return itensCertificate;
    }

    public void setItensCertificate(List<ReportItems> itensCertificate) {
        this.itensCertificate = itensCertificate;
    }

    public String getCnpjcpf() {
        return cnpjcpf;
    }

    public void setCnpjcpf(String cnpjcpf) {
        this.cnpjcpf = cnpjcpf;
    }



    public boolean getProximityToProtectedareas() {
        return ProximityToProtectedareas;
    }

    public void setProximityToProtectedareas(boolean proximityToProtectedareas) {
        ProximityToProtectedareas = proximityToProtectedareas;
    }

    public boolean getProximityToIndigenousLands() {
        return ProximityToIndigenousLands;
    }

    public void setProximityToIndigenousLands(boolean proximityToIndigenousLands) {
        ProximityToIndigenousLands = proximityToIndigenousLands;
    }

    public boolean getEmbargo() {
        return embargo;
    }

    public void setEmbargo(boolean embargo) {
        this.embargo = embargo;
    }

    public boolean getDeflorestation() {
        return deflorestation;
    }

    public void setDeflorestation(boolean deflorestation) {
        this.deflorestation = deflorestation;
    }

    public boolean getForcedLabor() {
        return forcedLabor;
    }

    public void setForcedLabor(boolean forcedLabor) {
        this.forcedLabor = forcedLabor;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


}

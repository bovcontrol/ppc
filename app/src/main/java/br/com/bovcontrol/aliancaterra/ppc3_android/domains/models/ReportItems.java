package br.com.bovcontrol.aliancaterra.ppc3_android.domains.models;

import android.content.Context;

import java.io.Serializable;

import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.StringUtils;

/**
 * Created by fernanda on 27/09/16.
 */
public class ReportItems implements Serializable {
    public String title;
    public boolean contained;
    public String explanation;
    public String font;//source change

    public ReportItems(String title, boolean contained, String explanation, String font) {
        this.title = title;
        this.contained = contained;
        this.explanation = explanation;
        this.font = font;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public String getExplanation(Context context) {
        return StringUtils.getStringAndSetParameters(context,explanation,context.getResources());
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public boolean isContained() {
        return contained;
    }

    public void setContained(boolean contained) {
        this.contained = contained;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}

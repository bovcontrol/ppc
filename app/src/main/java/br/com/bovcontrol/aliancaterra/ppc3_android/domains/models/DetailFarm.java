package br.com.bovcontrol.aliancaterra.ppc3_android.domains.models;

import android.content.Context;
import android.widget.EditText;

import com.google.firebase.database.Exclude;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import br.com.bovcontrol.aliancaterra.ppc3_android.app.SessionManager;

/**
 * Created by ricardoogliari on 10/1/16.
 */

public class DetailFarm {

    public String nameFarm;
    public String cpf_cnpj;
    public String address;
    public String point;
    public HashMap<String,Partner> partners= new HashMap<>();
    public String validate_promocode;
    public String request_promocode;
    public String payment_required;


    public DetailFarm() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public DetailFarm(String farm, String cpf_cnpj) {
        this.nameFarm = farm;
        this.cpf_cnpj = cpf_cnpj;

    }

    @Exclude
    public void setPointPosition(Position position) {
        this.point = position.position;
    }
    public void setPoint(String position) {
        this.point = position;
    }

    public void addPartner(String edCnpjCpf, String edPartnerName) {
        Partner objpartner= new Partner();
        objpartner.cpf_cnpj=edCnpjCpf;
        objpartner.name=edPartnerName;
        String  uniqueID = UUID.randomUUID().toString();
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        uniqueID+=""+timeStamp;
        uniqueID=uniqueID.replace("-","");
        partners.put(uniqueID,objpartner);
    }

    public void toMap(){

    }


}

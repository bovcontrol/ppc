package br.com.bovcontrol.aliancaterra.ppc3_android.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.UserInformations;
import br.com.bovcontrol.aliancaterra.ppc3_android.domains.models.ReportItems;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.ResourcesUtil;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.util.VectorDrawableUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fernanda on 27/09/16.
 */

public class ReportItemsAdapter extends RecyclerView.Adapter {

    List<ReportItems> list = new ArrayList<>();
    Context mcontext;
    ResourcesUtil res;
    VectorDrawableUtils vectorDrawableUtils;
    boolean setNewColor=false;
    int color;

    public ReportItemsAdapter(List<ReportItems> list, Context context) {
        this.mcontext = context;
        this.list = list;
        res=new ResourcesUtil();
        vectorDrawableUtils= new VectorDrawableUtils();
        this.setNewColor=false;
    }

    public ReportItemsAdapter(List<ReportItems> list, Context context,int color) {
        this.mcontext = context;
        this.list = list;
        res=new ResourcesUtil();
        vectorDrawableUtils= new VectorDrawableUtils();
        this.setNewColor=true;
        this.color=color;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.report_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewholder =(ViewHolder) holder ;
        ReportItems item=list.get(position);
        viewholder.itemreportExplanation.setText(item.getExplanation(mcontext));
        viewholder.itemReportSource.setText(mcontext.getString(res.getResourceId(mcontext,item.getTitle()+"_source","string")));
        viewholder.tvreportTitle.setText(mcontext.getString(res.getResourceId(mcontext,item.getTitle(),"string")));
        viewholder.tvreportResult.setText(UserInformations.isContained(mcontext,item.isContained()));


        String suffix="";
        if(item.isContained())
            suffix="_contained";

        if(item.isContained())
        {

            viewholder.item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int colorVisible=0;
                    int colorInvisible=0;
                    if((!setNewColor)) {
                        colorVisible = Color.parseColor("#D9A86A");
                        colorInvisible=Color.parseColor("#C9832A");
                    }else{
                        colorVisible = Color.parseColor("#F7F7F7");
                        colorInvisible=Color.parseColor("#FFFFFF");
                        viewholder.itemReportFontTitle.setTextColor(mcontext.getResources().getColor(R.color.colorAccent));
                        viewholder.itemreportExplanation.setTextColor(mcontext.getResources().getColor(R.color.colorBlackLight));
                        viewholder.itemReportSource.setTextColor(mcontext.getResources().getColor(R.color.colorBlackLight));
                    }
                    if (viewholder.llcontained.getVisibility() == View.GONE) {
                        viewholder.item.setBackgroundColor(colorVisible);
                        viewholder.llcontained.setVisibility(View.VISIBLE);
                    }else {
                        viewholder.item.setBackgroundColor(colorInvisible);
                        viewholder.llcontained.setVisibility(View.GONE);
                    }
                }
            });

        }
        Drawable drawable=vectorDrawableUtils.generateVactorDrawable(mcontext,res.getResourceId(mcontext,item.getTitle()+""+suffix,"drawable"));
        viewholder.imageViewIcon.setImageDrawable(drawable);
        if(setNewColor){
            viewholder.tvreportTitle.setTextColor(mcontext.getResources().getColor(R.color.colorBlackLight));
            if(item.isContained()) {
                viewholder.tvreportResult.setTextColor(mcontext.getResources().getColor(R.color.colorAccent));
                DrawableCompat.setTint(viewholder.imageViewIcon.getDrawable(), ContextCompat.getColor(mcontext, R.color.colorAccent));
            }else {
                viewholder.tvreportResult.setTextColor(mcontext.getResources().getColor(R.color.colorPrimary));
                DrawableCompat.setTint(viewholder.imageViewIcon.getDrawable(), ContextCompat.getColor(mcontext, R.color.colorPrimary));
            }
        }else{
            viewholder.tvreportResult.setTextColor(mcontext.getResources().getColor(R.color.white));
            DrawableCompat.setTint(viewholder.imageViewIcon.getDrawable(), ContextCompat.getColor(mcontext, R.color.white));

        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewIcon)
        ImageView imageViewIcon;
        @BindView(R.id.tvreportTitle)
        TextView tvreportTitle;
        @BindView(R.id.tvreportResult)
        TextView tvreportResult;
        @BindView(R.id.itemreport_explanation)
        TextView itemreportExplanation;
        @BindView(R.id.item_report_font_title)
        TextView itemReportFontTitle;
        @BindView(R.id.item_report_source)
        TextView itemReportSource;
        @BindView(R.id.llcontained)
        LinearLayout llcontained;
        @BindView(R.id.item)
        LinearLayout item;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

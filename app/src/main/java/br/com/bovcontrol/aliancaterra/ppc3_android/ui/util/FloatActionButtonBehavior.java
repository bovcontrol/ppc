package br.com.bovcontrol.aliancaterra.ppc3_android.ui.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;
import br.com.bovcontrol.aliancaterra.ppc3_android.ui.widgets.VerticalScrollingBehavior;

/**
 * Created by fernanda on 20/10/16.
 */

public class FloatActionButtonBehavior <V extends View> extends VerticalScrollingBehavior<V> {
    private static final Interpolator INTERPOLATOR = new LinearOutSlowInInterpolator();
    private int mMenuId;
    private boolean hidden = false;
    private ViewPropertyAnimatorCompat mTranslationAnimator;
    private FloatingActionButton mFloatActionMenu;
    private View mHolder;

    public FloatActionButtonBehavior() {
        super();
    }

    public FloatActionButtonBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.FloatActionButton_Params);
        mMenuId = a.getResourceId(R.styleable.FloatActionButton_Params_floatActionButtontId, View.NO_ID);
        a.recycle();
    }

    @Override
    public boolean onLayoutChild(CoordinatorLayout parent, V child, int layoutDirection) {
        boolean layoutChild = super.onLayoutChild(parent, child, layoutDirection);
        if (mFloatActionMenu == null && mMenuId != View.NO_ID) {
            mFloatActionMenu = findActionMenu(child);
            getTabsHolder();
        }
        return layoutChild;
    }

    private FloatingActionButton findActionMenu(View child) {
        if (mMenuId == 0) return null;
        return (FloatingActionButton) child.findViewById(mMenuId);
    }

    @Override
    public void onNestedVerticalOverScroll(CoordinatorLayout coordinatorLayout, V child, @ScrollDirection int direction, int currentOverScroll, int totalOverScroll) {
    }

    @Override
    public void onDirectionNestedPreScroll(CoordinatorLayout coordinatorLayout, V child, View target, int dx, int dy, int[] consumed, @ScrollDirection int scrollDirection) {
        handleDirection(child, scrollDirection);
    }

    private void handleDirection(V child, int scrollDirection) {
        if (scrollDirection == ScrollDirection.SCROLL_DIRECTION_DOWN && hidden) {
            hidden = false;
            mFloatActionMenu.show();
        } else if (scrollDirection == ScrollDirection.SCROLL_DIRECTION_UP && !hidden) {
            hidden = true;
            mFloatActionMenu.hide();
        }
    }

    @Override
    protected boolean onNestedDirectionFling(CoordinatorLayout coordinatorLayout, V child, View target, float velocityX, float velocityY, @ScrollDirection int scrollDirection) {
        handleDirection(child, scrollDirection);
        return true;
    }

    private void animateOffset(final V child, final int offset) {
        ensureOrCancelAnimator(child);
        mTranslationAnimator.translationY(offset).start();
        animateTabsHolder(offset);
    }

    private void animateTabsHolder(int offset) {
        if (mHolder != null) {
            offset = offset > 0 ? 0 : 1;
            ViewCompat.animate(mHolder).alpha(offset).setDuration(200).start();
        }
    }


    private void ensureOrCancelAnimator(V child) {
        if (mTranslationAnimator == null) {
            mTranslationAnimator = ViewCompat.animate(child);
            mTranslationAnimator.setDuration(100);
            mTranslationAnimator.setInterpolator(INTERPOLATOR);
        } else {
            mTranslationAnimator.cancel();
        }
    }

    private void getTabsHolder() {
        if (mFloatActionMenu != null) {
            mHolder = mFloatActionMenu;
        }
    }

    public static <V extends View> FloatActionButtonBehavior<V> from(V view) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        if (!(params instanceof CoordinatorLayout.LayoutParams)) {
            throw new IllegalArgumentException("The view is not a child of CoordinatorLayout");
        }
        CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams) params)
                .getBehavior();
        if (!(behavior instanceof FloatActionButtonBehavior)) {
            throw new IllegalArgumentException(
                    "The view is not associated with FloatActionButtonBehavior");
        }
        return (FloatActionButtonBehavior<V>) behavior;
    }
}

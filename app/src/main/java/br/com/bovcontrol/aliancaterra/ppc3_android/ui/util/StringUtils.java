package br.com.bovcontrol.aliancaterra.ppc3_android.ui.util;

import android.content.Context;
import android.content.res.Resources;
import android.provider.Settings;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.bovcontrol.aliancaterra.ppc3_android.R;

/**
 * Created by fernanda on 28/09/16.
 */

public class StringUtils {

    public static String getSalt() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return timeStamp;
    }

    public static String getStringAndSetParameters(Context context, String parameter,Resources resource){
        String stringformatted="";
        String[] split=parameter.split("\\|");
        if(split.length!=0){


            for (int i = 0; i < split.length; i++) {

                String sentence=split[i];
                String[] partsofsentence=sentence.split("/");

                String[] partsofsentenceDecoded=decode(partsofsentence);

                String cname=partsofsentenceDecoded[0];
                if(partsofsentence.length!=1) {
                    String plural = partsofsentenceDecoded[1];
                    String parameter1 = partsofsentence.length >= 3 ? partsofsentenceDecoded[2] : "";
                    String parameter2 = partsofsentence.length >= 4 ? partsofsentenceDecoded[3] : "";
                    String parameter3 = partsofsentence.length >= 5 ? partsofsentenceDecoded[4] : "";
                    String parameter4 = partsofsentence.length >= 6 ? partsofsentenceDecoded[5] : "";
                    ResourcesUtil res = new ResourcesUtil();
                    if(cname.equals("deforest") && (!plural.equals("sm") && (!plural.equals("pm")  && (!plural.equals("sf")  && (!plural.equals("pf")))))){
                        parameter1=plural;
                        plural="sm";

                    }
                    Log.d("string",""+cname+"_"+plural);
                    return String.format(resource.getString(res.getResourceId(context, cname+"_"+plural, "string")), parameter1, parameter2, parameter3, parameter4);
                }else
                    return "";



            }
        }

        return stringformatted;
    }

    private static String[] decode(String[] partsofsentence) {
        String[] decoded=new String[partsofsentence.length];
        int i=0;
        for(String item:partsofsentence){
            item=item.replaceAll("%2F","/");
            item=item.replaceAll("%7C","|");
            try {
               item= URLDecoder.decode(item, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            decoded[i]=item;
            i++;
        }
        return decoded;
    }
}
